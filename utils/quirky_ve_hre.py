'''
Usage:
    python3 utils/quirky_ve_hre.py public/techniky.xml senzor medi-ninja ...
reads public/techniky.xml and modifys it in place
'''
import sys
from xml.etree import ElementTree as ET
import html

techniky_xml_path = sys.argv[1]
in_game = sys.argv[2:]

document = ET.parse(techniky_xml_path)
for quirk in document.findall("quirk-list-mechanicky/quirk-mechanicky"):
    quirk.set("není-ve-hře", "false" if quirk.get("id") in in_game else "true")

# write
xml_string = ET.tostring(document.getroot()).decode('utf-8')
xml_string = html.unescape(xml_string)
with open(techniky_xml_path, 'w', encoding='utf-8') as document_file:
    document_file.write(xml_string)
