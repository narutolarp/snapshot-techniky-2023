# from  __future__ import annotations
from abc import abstractmethod
from itertools import filterfalse
import math
from os import PathLike
import types
from typing import Any, Callable, Iterable, Optional, Union
import typing
from xml.etree import ElementTree as ET
import xml.dom.minidom
import re
import sys
import html
import enum
import functools
import dataclasses
import collections

import unidecode


class TypEfektu(enum.Enum):
    Pasivni = 'Pasivní'
    Hlavni = 'Hlavní'
    Posileni = 'Posílení'
    Pomocna = 'Pomocná'
    Specialni = 'Speciální'

class TypEfektuMod(enum.Enum):
    Vylepseni = 'Vylepšení'
    Opakovatelna = 'Opakovatelná'
    Kombo = 'Kombo'
    Tymova = 'Týmová'
    Reaktivni = 'Reaktivní'
    HP = 'HP'
    Kouzlo = 'Kouzlo'

    @staticmethod
    def IsValid(mod: 'TypEfektuMod') -> bool:
        blacklist = [TypEfektuMod.Kombo, TypEfektuMod.Reaktivni, TypEfektuMod.Kouzlo, TypEfektuMod.HP]
        # TODO: blacklist Vylepseni?
        return mod not in blacklist


class Heslo(enum.Enum):
    Odhozeni = 'Odhození'
    Zmateni = 'Zmatení'
    Roztristeni = 'Roztříštění'
    Ochromeni = 'Ochromení'
    Paralyza = 'Paralýza'
    Uhyb = 'Úhyb'

class Zasah(enum.Enum):
    ruka = 'ruka'
    zbran_nablizko = 'zbraň-nablízko'
    zbran_nadalku = 'zbraň-nadálku'
    ryze = 'rýže'
    koule = 'koule'
    automaticky = 'automaticky'
    zbran = 'zbraň'
    nablizko = 'nablízko'
    nadalku = 'nadálku'
    vsechny = 'všechny'


STROM_TRANSLATE = {  # excel name to xml id
    'Zbraňové techniky': 'Bukijutsu',
    'Pečetící jutsu': 'Fuinjutsu',
    'Vítr': 'Fuuton',
    'Boj beze zbraně': 'Taijutsu', 
    'Oheň': 'Katon', 
    'Voda': 'Suiton', 
    'Blesk': 'Raiton', 
    'Yang': 'yang-styl', 
    'Formování čakry': 'Keitai', 
    'Země': 'Doton', 
    'Úskočnost': 'Kakuran', 
    'Příprava': 'Ningu', 
    'Yin': 'yin-styl',
    'Jinton': 'Jinton',
    }
for key, val in list(STROM_TRANSLATE.items()):
    STROM_TRANSLATE[key.lower()] = val

QUIRK_TRANSLATE = {  # excel name to xml id
    'Hyuga klan': 'hyuuga-klan',
    'Daiymio stráž': 'daimyo-straz',
    'parni-styl': 'styl-pary',
    'yang-imitace': 'yang-imitation',
    'zvucna-ctyrka-demonicka-fletna': 'demonicka-fletna',
    'zvucna-ctyrka-demonicky-pavouk': 'pavouci-techniky',
    'magneticy-styl': 'magneticky-styl',
    'zvucna-ctyrka-demonicke-vezeni': 'demon-prison',
    'zvucna-ctyrka-demonicka-dvojcata': 'twin-demon-technique',
    # 'kontrakt-ninken': '',
    }

def slugify(name: str) -> str:
    name_english = name.replace('čakr', 'chakr').replace('Čakr', 'chakr')
    return re.sub(r'[^a-zA-Z0-9]+', '-', unidecode.unidecode(name_english)).lower().strip()

for key, val in list(QUIRK_TRANSLATE.items()):
    QUIRK_TRANSLATE[slugify(key)] = val

def technika_name_to_id(name: str) -> str:
    slug = slugify(name)
    if slug.startswith('technika-'):
        return slug
    return 'technika-' + slug

def prep_value(val: str) -> str:
    val = val.strip()
    return val if val != '-' else ''

def prep_list(val: str) -> Iterable[str]:
    return filter(bool, map(prep_value, val.split(';')))


def add_text_child(parent, tag, text):
    if isinstance(text, Vsechny):
        text.render_xml(parent, tag)
        return
    el = ET.SubElement(parent, tag)
    el.text = str(text)

def reference(parent, tag, ref, content=None):
    if isinstance(ref, Vsechny):
        ref.render_xml(parent, tag)
        return
    if isinstance(ref, enum.Enum):
        ref = ref.value
    ref = str(ref)
    if ref not in STROM_TRANSLATE.values():
        ref = ref.lower()
    el = ET.SubElement(parent, tag)
    el.set('ref', ref)
    if content != None:
        el.text = str(content)


T = typing.TypeVar('T')
def iter_hodnota(heslo_value: dict[T, int], hesla: Optional[Iterable[T]]) -> Iterable[int]:
    return (heslo_value.get(heslo, 0) for heslo in (hesla or []))

T3 = typing.TypeVar('T3')
def sum_hodnota(heslo_value: dict[T3, int], hesla: Optional[Iterable[T3]]) -> int:
    return sum(iter_hodnota(heslo_value, hesla))

T2 = typing.TypeVar('T2')
def max_hodnota(heslo_value: dict[T2, int], hesla: Optional[Iterable[T2]]) -> int:
    return max([0, *iter_hodnota(heslo_value, hesla)])

HODNOTA_ZASAH = {
    Zasah.ruka: 0,
    Zasah.zbran: 1,
    Zasah.zbran_nablizko: 1,
    Zasah.zbran_nadalku: 1,
    Zasah.koule: 1,
    Zasah.ryze: 2,
    Zasah.automaticky: 5,
    Zasah.vsechny: 5,
}

HODNOTA_HESLO = {
    Heslo.Ochromeni: 3,
    Heslo.Odhozeni: 1,
    Heslo.Roztristeni: 4,
    Heslo.Paralyza: 3,
    Heslo.Zmateni: 2,
    Heslo.Uhyb: 5,
}

class Vsechny:
    NAME_MAP = {
        'heslo': 'hesla',
    }

    def __init__(self, value):
        if value != 'všechny':
            raise ValueError()

    def __str__(self):
        return 'všechny'
    
    def render_xml(self, parent: ET.Element, output_path: str) -> None:
        ref = None
        if output_path.startswith('ref:'):
            output_path_split = output_path.split(':')
            if len(output_path_split) == 3:
                [_, output_path, ref] = output_path.split(':')
            elif len(output_path_split) == 2:
                [_, output_path] = output_path.split(':')
        name = output_path.strip('@').split(':')[0]
        name = self.NAME_MAP.get(name, name)
        el = ET.SubElement(parent, f'všechny-{name}')
        if ref:
            el.set('ref', ref)


###########
# PARSING #

try_transforms = (
    lambda x: x,
    lambda x: x.title(),
    lambda x: x.lower(),
)


@dataclasses.dataclass
class RowParser:
    _row_id: int
    _get_cell_value: dataclasses.InitVar[Callable[[Optional[str], str], str]]

    def __post_init__(self, _get_cell_value: Callable[[Optional[str], str], str]) -> None:
        for field in dataclasses.fields(self):
            types_to_check = [field.type]
            if isinstance(field.type, types.UnionType):
                types_to_check = typing.get_args(field.type)
            if field.name.startswith('_'):
                continue
            head_keys = field.metadata['head_keys']
            value = _get_cell_value(*head_keys)
            if value == '':
                assert field.default != REQUIRED_CELL, f'{self} missing required column "{head_keys[1]}"'
                continue

            first_exception = None
            value_set = False
            for type_ in types_to_check:
                is_list = False
                if isinstance(type_, types.GenericAlias):
                    is_list = typing.get_origin(type_) is list
                    type_ = typing.get_args(type_)[0]
                try:
                    if is_list:
                        new_value = [self._validate(v, type_) for v in prep_list(value)]
                    else:
                        new_value = self._validate(value, type_)
                except ValueError as e:
                    if first_exception is None:
                        first_exception = e
                    continue
                setattr(self, field.name, new_value)
                value_set = True
                break
            if not value_set:
                assert first_exception, 'should never be empty'
                raise first_exception

    def __str__(self) -> str:
        return f'<{type(self).__name__}[řádek {self._row_id}]>'

    def _validate(self, value, type_):
        first_exception = None
        for transform in try_transforms:
            try:
                # print(type_)
                return type_(transform(value))
            except Exception as e:
                if first_exception == None:
                    first_exception = e
        raise ValueError(f'{self} has invalid value: {value} (type: {type_})') from first_exception

    def has_group(self, prefix: str) -> bool:
        for field in dataclasses.fields(self):
            if field.name.startswith(f'{prefix}_'):
                if getattr(self, field.name):
                    return True
        return False
    
    def xml_append_into(self, parent: ET.Element) -> None:
        root = ET.SubElement(parent, self.get_tag_name())
        # root.set('dbg-row-id', str(self._row_id))
        for field in dataclasses.fields(self):
            output_path: Union[None, str, object] = field.metadata.get('output_path')
            default_when_used = field.metadata.get('default_when_used')
            value_raw = getattr(self, field.name)
            if value_raw is None and default_when_used is not None:
                value_raw = default_when_used
            if field.name.startswith('_') or not output_path or value_raw is None:
                continue
            if output_path is MANUAL:
                self.handle_xml_cell_special(field.name, root)
                continue
            assert isinstance(output_path, str)
            output_map: Callable = field.metadata['output_map']
            output_filter: Callable = field.metadata['output_filter']
            val_list = value_raw if isinstance(value_raw, list) else [value_raw]
            for value in val_list:
                if isinstance(value, Vsechny):
                    value.render_xml(root, output_path)
                    continue
                if not output_filter(value):
                    continue
                if isinstance(value, enum.Enum):
                    value = value.value
                value = str(output_map(value))
                if output_path.startswith('@'):
                    root.set(output_path[1:], value)
                elif output_path.startswith('ref:'):
                    parts = output_path.split(':')
                    if len(parts) == 3:
                        reference(root, parts[1], parts[2], value)
                    elif len(parts) == 2:
                        reference(root, parts[1], value)
                    else:
                        assert False
                else:
                    add_text_child(root, output_path, value)
        self.handle_xml_suffix(root)

    @abstractmethod
    def handle_xml_cell_special(self, field_name: str, root: ET.Element) -> None:
        raise NotImplementedError()
    
    def handle_xml_suffix(self, root: ET.Element) -> None:
        pass
    
    @abstractmethod
    def get_tag_name(self) -> str:
        raise NotImplementedError()
    
    @abstractmethod
    def do_computations(self) -> None:
        raise NotImplementedError()



MANUAL = object()
REQUIRED_CELL = object()

def cell(head1: Union[str, None], head2: str, output_path: Union[None, str, object] = None, *, default: Any = REQUIRED_CELL, output_map = None, output_filter = None, default_when_used = None):
    return dataclasses.field(
        default=default,
        init=False,
        metadata={
            'head_keys': (head1, head2),
            'output_path': output_path,
            'output_map': output_map or (lambda x: x),
            'output_filter': output_filter or (lambda _: True),
            'default_when_used': default_when_used,
        },
    )



##########
# EFEKTY #

@dataclasses.dataclass
class Efekt(RowParser):
    zdvojit_cenu: str = cell(None, 'zdvojit cenu', MANUAL, default=None)
    upgrades: str = cell(None, 'vylepšuje techniku', 'ref:vylepšuje-techniku', default=None, output_map=technika_name_to_id)
    afterefect_list: list[Heslo] = cell(None, 'Efekt na sesilatele', 'ref:efekt-na-sesilatele', default=None)
    plaintext: str = cell(None, 'co to dělá nebo upravuje (plaintext)', default=None)
    
    def handle_xml_cell_special(self, field_name: str, root: ET.Element) -> None:
        if field_name == 'zdvojit_cenu':
            ET.SubElement(root, 'zdvojit-cenu')

    @staticmethod
    def CreateEfects(_row_id: int, _get_cell_value) -> Iterable['Efekt']:
        efekty = []
        for cls in Efekt.__subclasses__():
            if cls.__name__.endswith('Mixin'):
                continue
            ef = cls(_row_id, _get_cell_value)  # type: ignore
            if ef.is_filled():
                efekty.append(ef)
        # kam patri plaintext
        if len(efekty) == 0:  # plaintext je jedina vec naradku, je tedy standalone
            spec_ef = EfektSpecial(_row_id, _get_cell_value)
            if spec_ef.plaintext and spec_ef.plaintext.strip():
                efekty.append(spec_ef)
        elif len(efekty) == 1:  # plaintext je pouze s pasivkou, je tedy standalone
            ef = efekty[0]
            if isinstance(ef, EfektPasivni):
                spec_ef = EfektSpecial(_row_id, _get_cell_value)
                if spec_ef.plaintext and spec_ef.plaintext.strip():
                    efekty.append(spec_ef)
                ef.plaintext = None  # type: ignore
        else:  # plaintext patri k ne-pasivnim efektum
            for ef in efekty:
                if isinstance(ef, EfektPasivni):
                    ef.plaintext = None  # type: ignore
        # obecne upravy
        for ef in efekty:
            if isinstance(ef, EfektPasivni):
                ef.afterefect_list = None #type: ignore
        return efekty

    @abstractmethod
    def is_filled(self) -> bool:
        raise NotImplementedError()
    
    def handle_xml_suffix(self, root: ET.Element) -> None:
        if self.plaintext:
            spec = ET.SubElement(root, 'speciální')
            spec.text = self.plaintext

    def do_computations(self) -> None:
        pass

    @abstractmethod
    def get_cena_hodnota(self) -> int:
        raise NotImplementedError()


@dataclasses.dataclass
class EfektPasivni(Efekt):
    add_chakra: int = cell('pasivni (technika)', '+chakra', 'ref:atribut:max-chakra', default=None)
    add_hp: int = cell('pasivni (technika)', '+životy', 'ref:atribut:max-život', default=None)
    add_weapon: int = cell('pasivni (technika)', '+cm zbran', 'ref:atribut:max-zbraň-cm', default=None)
    sleva_target_tree: list[str] = cell('zlevnění ceny (pasivni, vylepseni) (technika)', 'plošně', MANUAL, default=None)
    sleva_target_technika: list[str] = cell('zlevnění ceny (pasivni, vylepseni) (technika)', 'technika', MANUAL, default=None)
    sleva_pecet: int = cell('zlevnění ceny (pasivni, vylepseni) (technika)', 'pečetě', default=None)
    sleva_chakra: int = cell('zlevnění ceny (pasivni, vylepseni) (technika)', 'chakra', default=None)
    sleva_hp: int = cell('zlevnění ceny (pasivni, vylepseni) (technika)', 'životy', default=None)

    def get_tag_name(self) -> str:
        return 'pasivní'
    
    def is_filled(self) -> bool:
        return self.has_group('add') or self.has_group('sleva')
    
    def handle_xml_cell_special(self, field_name: str, root: ET.Element) -> None:
        if field_name == 'sleva_target_tree':
            assert not self.sleva_target_technika
            self._add_sleva_to_xml(root)
        elif field_name == 'sleva_target_technika':
            assert not self.sleva_target_tree
            self._add_sleva_to_xml(root)
        else:
            super().handle_xml_cell_special(field_name, root)

    def get_cena_hodnota(self) -> int:
        return (
            (self.add_chakra or 0) * 1
            + (self.add_hp or 0) * 4
            + ((self.add_weapon or 0) // 30) * 2
        )

    def _add_sleva_to_xml(self, root: ET.Element) -> None:
        sleva = ET.SubElement(root, 'sleva')
        # selektor
        if self.sleva_target_tree and ('všechny' in self.sleva_target_tree or 'Všechny' in self.sleva_target_tree):
            assert len(self.sleva_target_tree) == 1
            ET.SubElement(sleva, 'všechny-techniky')
        elif self.sleva_target_tree:
            for strom in self.sleva_target_tree:
                reference(sleva, 'strom', STROM_TRANSLATE.get(strom, strom))
        elif self.sleva_target_technika:
            for technika_jmeno in self.sleva_target_technika:
                reference(sleva, 'technika', technika_name_to_id(technika_jmeno))
        else:
            assert False, str([self.sleva_target_tree, self.sleva_target_technika])
        # hodnota slevy
        if self.sleva_pecet:
            reference(sleva, 'cena', 'pečeť', self.sleva_pecet)
        if self.sleva_chakra:
            reference(sleva, 'cena', 'chakra', self.sleva_chakra)
        if self.sleva_hp:
            reference(sleva, 'cena', 'život', self.sleva_hp)


@dataclasses.dataclass
class FollowUpMixin(Efekt):
    followup_req_zasah: list[Zasah] = cell('follow-up (utocne/kombo)', 'req typ zasahu', MANUAL, default=None)
    followup_req_dmg: int = cell('follow-up (utocne/kombo)', 'req dmg', MANUAL, default=None)
    followup_req_hesla: list[Heslo] = cell('follow-up (utocne/kombo)', 'req hesla', MANUAL, default=None)

    def handle_xml_suffix(self, root: ET.Element) -> None:
        if self.followup_req_zasah or self.followup_req_dmg or self.followup_req_hesla:
            podminky = ET.SubElement(root, 'musí-následovat-zásah')
            if self.followup_req_zasah:
                for zasah in self.followup_req_zasah:
                    reference(podminky, 'zásah', zasah.value)
            else:
                reference(podminky, 'zásah', Zasah.vsechny)
            if self.followup_req_dmg:
                ET.SubElement(podminky, 'zranění').text = str(self.followup_req_dmg)
            if self.followup_req_hesla:
                for heslo in self.followup_req_hesla:
                    reference(podminky, 'heslo', heslo.value)
        super().handle_xml_suffix(root)


@dataclasses.dataclass
class EfektKombo(FollowUpMixin, Efekt):
    kombo_typ_zasahu: list[Zasah] = cell('buff (stare kombo)', 'typ zasahu', 'ref:zásah', default=None, default_when_used=Zasah.vsechny)
    kombo_plus_dmg: int = cell('buff (stare kombo)', 'plus dmg', 'zranění', default=None)
    kombo_plus_hesla: list[Heslo] = cell('buff (stare kombo)', 'plus hesla', 'ref:heslo', default=None)
    kombo_req_dmg: int = cell('buff (stare kombo)', 'req min dmg', MANUAL, default=None)
    kombo_req_efekt: list[Heslo] = cell('buff (stare kombo)', 'req hesla', MANUAL, default=None)

    def get_tag_name(self) -> str:
        return 'kombo'
    
    def is_filled(self) -> bool:
        return self.has_group('kombo')

    def handle_xml_suffix(self, root: ET.Element) -> None:
        if self.kombo_req_dmg or self.kombo_req_efekt:
            podminky = ET.SubElement(root, 'podmínky')
            if self.kombo_req_dmg:
                ET.SubElement(podminky, 'zranění').text = str(self.kombo_req_dmg)
            if self.kombo_req_efekt:
                for heslo in self.kombo_req_efekt:
                    reference(podminky, 'heslo', heslo.value)
        return super().handle_xml_suffix(root)
    
    def get_cena_hodnota(self) -> int:
        return EfektUtocna.compute_cena_hodnota(self.kombo_plus_dmg, self.kombo_plus_hesla, self.has_group('followup'))


@dataclasses.dataclass
class EfektUtocna(FollowUpMixin, Efekt):
    utocna_zasah: list[Zasah] = cell('utocene', 'typy zasahu', 'ref:zásah', default=None, default_when_used=Zasah.vsechny)
    utocna_dmg: int = cell('utocene', 'dmg', 'zranění', default=None)
    utocna_hesla: list[Heslo] = cell('utocene', 'hesla', 'ref:heslo', default=None)

    def get_tag_name(self) -> str:
        return 'útočná'
    
    def is_filled(self) -> bool:
        return self.has_group('utocna')
    
    def get_cena_hodnota(self) -> int:
        return self.compute_cena_hodnota(self.utocna_dmg, self.utocna_hesla, self.has_group('followup')) + max_hodnota(HODNOTA_ZASAH, self.utocna_zasah)
    
    @staticmethod
    def compute_cena_hodnota(dmg: Optional[int], hesla: Optional[list[Heslo]], has_followup: bool) -> int:
        dmg_coef = 2
        base_hodnota = sum_hodnota(HODNOTA_HESLO, hesla) + dmg_coef * (dmg or 0)
        followup_koef = 0.8 if has_followup else 1
        return max(1, math.floor(followup_koef * base_hodnota))


@dataclasses.dataclass
class EfektObranna(Efekt):
    obranna_zasah: list[Zasah] = cell('obranny', 'typy zasahu', 'ref:zásah', default=None, default_when_used=Zasah.vsechny)
    obranna_dmg: int = cell('obranny', 'dmg', 'zranění', default=None)
    obranna_hesla: list[Heslo] = cell('obranny', 'hesla', 'ref:heslo', default=None)
    obranna_stin: str = cell('obranny', 'stin zivot', MANUAL, default=None)

    def get_tag_name(self) -> str:
        return 'obranná'
    
    def is_filled(self) -> bool:
        return self.has_group('obranna')
    
    def handle_xml_cell_special(self, field_name: str, root: ET.Element) -> None:
        if field_name == 'obranna_stin':
            ET.SubElement(root, 'stínový-život')
        super().handle_xml_cell_special(field_name, root)

    def get_cena_hodnota(self) -> int:
        dmg_coef = 2
        return (
            max_hodnota(HODNOTA_ZASAH, self.obranna_zasah)
            + sum_hodnota(HODNOTA_HESLO, self.obranna_hesla)
            + (self.obranna_dmg or 0) * dmg_coef
            + (4 if self.obranna_stin else 0)
        )


@dataclasses.dataclass
class EfektLeceni(Efekt):
    leceni_dmg: int | Vsechny = cell('plaintext (leceni)', 'pocet zraneni', 'zranění', default=None)
    leceni_hesla: list[Heslo] | Vsechny = cell('plaintext (leceni)', 'hesla ktera odstrani', 'ref:heslo', default=None)

    def get_tag_name(self) -> str:
        return 'léčení'
    
    def is_filled(self) -> bool:
        return self.has_group('leceni')
    
    def get_cena_hodnota(self) -> int:
        if isinstance(self.leceni_hesla, Vsechny) or isinstance(self.leceni_dmg, Vsechny):
            return 0
        hodnota_leceni_zivotu = 4
        hodnota_leceni_hesla = { Heslo.Ochromeni: 3, Heslo.Roztristeni: 5, Heslo.Paralyza: 3, Heslo.Zmateni: 2 }
        return sum_hodnota(hodnota_leceni_hesla, self.leceni_hesla) + (self.leceni_dmg or 0) * hodnota_leceni_zivotu


@dataclasses.dataclass
class EfektNeviditelnost(Efekt):
    neviditelnost_lvl: int = cell('plaintext (neviditelnost)', 'uroven', 'úroveň', default=None)

    def get_tag_name(self) -> str:
        return 'neviditelnost'
    
    def is_filled(self) -> bool:
        return self.has_group('neviditelnost')
    
    def get_cena_hodnota(self) -> int:
        if self.neviditelnost_lvl:
            return self.neviditelnost_lvl + 3
        return 0


@dataclasses.dataclass
class EfektVnimani(Efekt):
    vnimani_lvl: int = cell('plaintext (vnimani)', 'uroven', 'úroveň', default=None)
    vnimani_typ: str = cell('plaintext (vnimani)', 'typ', 'ref:typ-vnímání', output_map=slugify, default=None)

    def get_tag_name(self) -> str:
        return 'vnímání'
    
    def is_filled(self) -> bool:
        return self.has_group('vnimani')
    
    def get_cena_hodnota(self) -> int:
        if self.vnimani_lvl:
            return self.vnimani_lvl + 3
        return 0


@dataclasses.dataclass
class EfektSpecial(Efekt):
    def get_tag_name(self) -> str:
        return 'speciální'
    
    def is_filled(self) -> bool:
        return False  # tvoren manualne, neni parsovan automaticky

    def get_cena_hodnota(self) -> int:
        return 0



############
# TECHNIKA #

@dataclasses.dataclass
class Technika(RowParser):
    _efekty: list[Efekt] = dataclasses.field(default_factory=list)
    _id_suffix: Optional[int] = dataclasses.field(default=None, init=False)
    jmeno: str = cell(None, 'Jméno', 'jméno')
    jmeno_jap: str = cell(None, 'Japonské jméno', 'alias', default=None)
    level: int = cell(None, 'lvl', 'úroveň')
    typ: TypEfektu = cell(None, 'typ', 'ref:typ-efektu')
    typ_modifikator_list: list[TypEfektuMod] = cell(None, 'typ-modifikátor', 'ref:modifikátor-efektu', default=None, output_filter=TypEfektuMod.IsValid)
    strom: str = cell(None, 'strom')
    cena_pecet: int = cell('cena (technika)', 'pečeť', 'ref:cena:pečeť', default=None, output_filter=bool)
    cena_chakra: int | Vsechny = cell('cena (technika)', 'chakra', MANUAL, default=None, output_filter=bool)
    cena_zivot: int | Vsechny = cell('cena (technika)', 'život', 'ref:cena:život', default=None, output_filter=bool)
    cooldown: str = cell(None, 'cooldown', 'ref:cooldown', default=None, output_map=lambda x: 'jednou' if x.lower() == 'hra' else x)
    quirk: list[str] = cell(None, 'quirk', MANUAL, default=())
    prerekvizity: list[str] = cell(None, 'vyžaduje techniku', MANUAL, default=None)
    training: str = cell(None, 'kategorie učení', 'tréninková-lokace', default=None)
    flavour: str = cell(None, 'flavour', 'flavour', default='')

    @property
    def id(self):
        base_id = technika_name_to_id(self.jmeno)
        if self._id_suffix is None:
            return base_id
        return base_id + f"-{self._id_suffix}"

    def add_efekt(self, efekt: Efekt) -> None:
        assert isinstance(efekt, Efekt)
        if self.typ_modifikator_list is not None:
            if bool(efekt.upgrades) != (TypEfektuMod.Vylepseni in self.typ_modifikator_list):
                print(f'WARN: vylepseni: {efekt} \t<- {self.jmeno}')
        self._efekty.append(efekt)

    def get_tag_name(self) -> str:
        return 'technika'
    
    def handle_xml_cell_special(self, field_name: str, root: ET.Element) -> None:
        if field_name == 'quirk':
            if not self.quirk:
                ET.SubElement(root, 'veřejná')
            else:
                for q in self.quirk:
                    ET.SubElement(root, 'quirková').set('ref', QUIRK_TRANSLATE.get(slugify(q), slugify(q)))
        if field_name == 'prerekvizity':
            prerekvizity_el = ET.SubElement(root, 'prerekvizity')
            for req in self.prerekvizity:
                reference(prerekvizity_el, 'technika', technika_name_to_id(req))
        if field_name == 'cena_chakra':
            if self.typ_modifikator_list and TypEfektuMod.HP in self.typ_modifikator_list:
                reference(root, 'cena', 'život', self.cena_chakra)
            else:
                reference(root, 'cena', 'chakra', self.cena_chakra)

    def handle_xml_suffix(self, root: ET.Element) -> None:
        root.set('id', self.id)
        for efekt in self._efekty:
            efekt.xml_append_into(root)

    def do_computations(self) -> None:
        if self.has_group('cena'):
            return  # already defined in excel
        if all(map(lambda e: isinstance(e, (EfektPasivni, EfektSpecial)), self._efekty)):
            return
        if self.typ in (TypEfektu.Pasivni, TypEfektu.Specialni):
            return
        hodnoty = []
        for efekt in self._efekty:
            h = efekt.get_cena_hodnota()
            if efekt.zdvojit_cenu:
                h = math.ceil(h / 2)
            if isinstance(efekt, EfektPasivni):
                # hodnota_pasivni = h
                continue
            if self.typ_modifikator_list and TypEfektuMod.Tymova in self.typ_modifikator_list:
                h += 2
            h -= sum_hodnota(HODNOTA_HESLO, efekt.afterefect_list)
            if h > 0:
                hodnoty.append(h)
        hodnota = 0
        if hodnoty:
            hodnota = math.ceil(sum(hodnoty) / len(hodnoty))
            hodnota -= self.level - 1
            hodnota = max(hodnota, 1)
        if hodnota > 0:
            if self.typ_modifikator_list and TypEfektuMod.HP in self.typ_modifikator_list:
                self.cena_zivot = math.ceil(hodnota / 3)
            elif self.typ_modifikator_list and TypEfektuMod.Reaktivni in self.typ_modifikator_list:
                self.cena_chakra = hodnota
            elif self.typ_modifikator_list and TypEfektuMod.Kouzlo in self.typ_modifikator_list:
                self.cena_chakra = math.ceil(hodnota * 1 / 4) or 1
                self.cena_pecet = math.floor(hodnota * 3 / 4) + 1 or 2
            else:
                self.cena_chakra = math.ceil(hodnota * 2 / 4) or 1
                self.cena_pecet = math.floor(hodnota * 3 / 4) or 1



###################
# LOADING/PARSING #

def load(tsv_path: str) -> list[list[str]]:
    with open(tsv_path, encoding="utf-8") as tsv:
        return [row.split('\t') for row in tsv]


def get_cell_value_full(row: list[str], head1: list[str], head2: list[str], head1_key: Optional[str], head2_key: str) -> str:
    start_index = 0
    end_index = 999
    if head1_key != None:
        start_index = head1.index(head1_key)
        for end_index in range(start_index+1, len(head1)):
            if head1[end_index] != '':
                break
    index = head2.index(head2_key, start_index, end_index+1)
    return prep_value(row[index])


_excel_quirk_strom: set[tuple[str, str]] = set()
_excel_quirky: set[str] = set()
def parse(data: list[list[str]]) -> list[Technika]:
    head1 = [x.strip() for x in data[0]]
    head2 = [x.strip() for x in data[1]]
    
    techniky: list[Technika] = []
    last_technika = None
    for i in range(2, len(data)):
        row = data[i]
        _get_cell_value = functools.partial(get_cell_value_full, row, head1, head2)
        if _get_cell_value(None, 'Jméno') == 'END':
            break
        if _get_cell_value(None, 'lvl') == 'SKIP':
            continue
        if _get_cell_value(None, 'quirk'):
            quirk = _get_cell_value(None, 'quirk').split(';')
            strom = _get_cell_value(None, 'strom')
            for q in quirk:
                q_slug = slugify(q.strip())
                q_ = QUIRK_TRANSLATE.get(q_slug, q_slug)
                _excel_quirky.add(q_)
                _excel_quirk_strom.add((q_, STROM_TRANSLATE.get(strom, slugify(strom))))
        if _get_cell_value(None, 'Jméno') != '':
            techniky.append(last_technika := Technika(i + 1, _get_cell_value))
        assert last_technika
        for efekt in Efekt.CreateEfects(i + 1, _get_cell_value):
            last_technika.add_efekt(efekt)
    return techniky


def encode(techniky: list[Technika], prev: ET.ElementTree) -> None:
    for technika in techniky:
        found = False
        tree_id = STROM_TRANSLATE.get(technika.strom, slugify(technika.strom))
        for tree_el in prev.findall("strom"):
            if tree_id != tree_el.attrib['id']:
                continue
            technika.xml_append_into(tree_el)
            found = True
        assert found, f'{technika}: Tree not found in xml: "{tree_id}" (excel name: "{technika.strom}")'



########
# MAIN #

if __name__ == '__main__':
    DOCUMENT_PATH = '../../public/techniky.xml'
    data = load('techniky_detail.tsv')
    document = ET.parse(DOCUMENT_PATH)
    # turn into xml structure
    techniky = parse(data)
    # extra processing
    for technika in techniky:
        technika.do_computations()
    # handle duplicit names
    id_counter: collections.Counter[str] = collections.Counter()
    for technika in techniky:
        id_counter.update([technika.id])
        if id_counter[technika.id] > 1:
            technika._id_suffix = id_counter[technika.id]
    # remove old techniky
    _xml_quirk_strom: set[tuple[str, str]] = set()
    _xml_quirky: set[str] = set()
    for tree_el in document.findall("strom"):
        for technika_el in tree_el.findall("technika"):
            if not technika_el.get('id', '').startswith('quirk-placeholder'):
                tree_el.remove(technika_el)
            else:
                for quirk_el in technika_el.findall('quirková'):
                    _xml_quirky.add(quirk_el.attrib['ref'])
                    _xml_quirk_strom.add((quirk_el.attrib['ref'], tree_el.attrib['id']))
    # add techniky into document
    encode(techniky, document)
    #kontrola quirku
    # print('\n# XML #', *sorted(_xml_quirky), sep='\n')
    # print('\n# EXCEL #', *sorted(_excel_quirky), sep='\n')
    xml_not_in_excel = _xml_quirky.difference(_excel_quirky)
    excel_not_in_xml = _excel_quirky.difference(_xml_quirky)
    xml_pair_not_in_excel = _xml_quirk_strom.difference(_excel_quirk_strom)
    excel_pair_not_in_xml = _excel_quirk_strom.difference(_xml_quirk_strom)
    with open('chybejici_quirky.csv', 'w') as file:
        for q in xml_not_in_excel:
            file.write(q + ',(v kteremkoli strome),(je v databazi ale neni v excelu)\n')
        for q in excel_not_in_xml:
            file.write(q + ',(v kteremkoli strome),(je v excelu ale neni v databazi)\n')
        for q, t in xml_pair_not_in_excel:
            if q in xml_not_in_excel:
                continue
            file.write(f"{q},{t},(je v databazi ale neni v excelu)\n")
        for q, t in excel_pair_not_in_xml:
            if q in excel_not_in_xml:
                continue
            file.write(f"{q},{t},(je v excelu ale neni v databazi)\n")
    # output
    xml_string = ET.tostring(document.getroot()).decode('utf-8')
    xml_string = html.unescape(xml_string)
    with open(DOCUMENT_PATH, 'w', encoding='utf-8') as document_file:
        document_file.write(xml_string)
    # document.write(sys.stdout)
    # print(xml_string)

    # dom = xml.dom.minidom.parseString(xml_string)
    # print(dom.toprettyxml())
