if [ "$1" = "" ]  # first param is the elm executable
then
    ELM="elm"
else
    ELM="$1"
fi

if [ "$2" = "prod" ]  # second param can toggle production build
then
    FLAGS="--optimize"
    cp src/Env/Env.elm src/Env/tmp-backup-Env.elm
    cp src/Env/Prod.elm src/Env/Env.elm
    sed 's/Env.Prod/Env.Env/' src/Env/Env.elm > tmp_src_End  # -i is incompatible on alpine
    mv tmp_src_End src/Env/Env.elm
else
    FLAGS="--debug"
fi

FAILED="0"
for f in src/App/*.elm
do
    NAME=$(basename -s .elm "$f")
    if $ELM make $FLAGS "$f" --output "../public/bundle-$NAME.js"
    then
        echo "build of $NAME: OK"
    else
        echo "build of $NAME: ERROR"
        FAILED="1"
    fi
done

if [ -f src/Env/tmp-backup-Env.elm ]
then
    mv src/Env/tmp-backup-Env.elm src/Env/Env.elm
fi

if [ "$FAILED" = "1" ]
then
    exit 1
fi
