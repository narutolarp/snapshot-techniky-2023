
Folder structure
* src/ - entities live here, they only import UI or each other
* src/App/ - entrypoints - noone imports them, they can import anyone
* src/UI/ - "stupid" view functions, they don't import anyone, and everyone can import them
* src/Usecase/ - logical pieces that combine multiple entitites, only App can import them

So the import hierarchy is something like this:
* src/App/
    * src/Usecase/
        * src/
            * src/UI/
