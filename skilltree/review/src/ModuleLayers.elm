module ModuleLayers exposing (moduleLayerRule)

import Fork.NoViolationOfModuleLayerDependency
    exposing
        ( ModuleLayer(..)
        , ModuleLayerDependency(..)
        )


moduleLayerRule : ModuleLayerDependency
moduleLayerRule =
    ModuleLayerDependency
        [ DefaultLayer
        , uiLayer
        , sharedEntityLayer
        , entityLayer
        , usecaseLayer
        , appLayer
        ]



uiLayer : ModuleLayer
uiLayer =
    ModuleLayer
        [ [ "UI", "*" ]
        , [ "UI" ]
        , [ "Utils" ]
        ]


sharedEntityLayer : ModuleLayer
sharedEntityLayer =
    ModuleLayer
        [ [ "GUID" ]
        , [ "DatabaseCache" ]
        , [ "Api" ]
        ]


entityLayer : ModuleLayer
entityLayer =
    ModuleLayerMutuallyExclusive
        [ [ "*" ]
        ]


usecaseLayer : ModuleLayer
usecaseLayer =
    ModuleLayer
        [ [ "Usecase", "*" ]
        ]


appLayer : ModuleLayer
appLayer =
    ModuleLayerMutuallyExclusive
        [ [ "App", "*" ]
        ]
