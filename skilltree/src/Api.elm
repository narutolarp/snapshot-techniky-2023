module Api exposing (FetchOptions, fetch, sand)

import Http
import Xml.Decode


type alias FetchOptions response msg =
    { msg : Result String response -> msg
    , url : String
    , decoder : Xml.Decode.Decoder response
    }


fetch : FetchOptions response msg -> Cmd msg
fetch options =
    request
        { method = "GET"
        , url = options.url
        , body = Http.emptyBody
        , decoder = options.decoder
        , msg = options.msg
        }


sand : FetchOptions response msg -> String -> Cmd msg
sand options body =
    request
        { method = "PUT"
        , url = options.url
        , body = Http.stringBody "application/xml" body
        , decoder = options.decoder
        , msg = options.msg
        }


type alias RequestParams response msg =
    { method : String
    , url : String
    , body : Http.Body
    , msg : Result String response -> msg
    , decoder : Xml.Decode.Decoder response
    }


request : RequestParams response msg -> Cmd msg
request options =
    Http.request
        { method = options.method
        , headers = []
        , url = options.url
        , body = options.body
        , expect =
            Http.expectString <|
                Result.mapError errorToString
                    >> Result.andThen (Xml.Decode.run options.decoder)
                    >> options.msg
        , timeout = Nothing
        , tracker = Nothing
        }


errorToString : Http.Error -> String
errorToString error =
    case error of
        Http.BadUrl url ->
            "The URL " ++ url ++ " was invalid"

        Http.Timeout ->
            "Unable to reach the server, try again"

        Http.NetworkError ->
            "Unable to reach the server, check your network connection"

        Http.BadStatus 500 ->
            "The server had a problem, try again later"

        Http.BadStatus 400 ->
            "Verify your information and try again"

        Http.BadStatus _ ->
            "Unknown error"

        Http.BadBody errorMessage ->
            errorMessage
