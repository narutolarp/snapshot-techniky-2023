module App.Table exposing (Data, Model, Msg, main)

import Browser
import Css
import DatabaseCache exposing (DatabaseCache)
import GUID
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import Jutsu exposing (Jutsu)
import JutsuAccess exposing (JutsuAccess)
import QuirkMechanicky exposing (QuirkMechanicky)
import Tree exposing (Tree)
import UI
import Usecase.TechnikyDocument
import Utils exposing (ListNotEmpty)


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , view = Html.toUnstyled << view
        , update = update
        , subscriptions = always Sub.none
        }


type alias Model =
    { cache : DatabaseCache
    , data : Data
    }


type Data
    = Loading
    | Error String
    | Data DataPayload


type alias DataPayload =
    { techniky : List Jutsu
    , quirky : ListNotEmpty QuirkMechanicky
    , stromy : ListNotEmpty Tree
    }



-- INIT


init : () -> ( Model, Cmd Msg )
init _ =
    ( { cache = DatabaseCache.empty
      , data = Loading
      }
    , Usecase.TechnikyDocument.fetch GotTechnikyDokument
    )



-- UPDATE


type Msg
    = GotTechnikyDokument (Result String Usecase.TechnikyDocument.TechnikyDocument)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotTechnikyDokument (Ok doc) ->
            case ( doc.quirky, doc.stromy ) of
                ( q :: qs, s :: ss ) ->
                    ( { model
                        | data =
                            Data
                                { techniky = List.concatMap Tree.techniky doc.stromy
                                , quirky = ( q, qs )
                                , stromy = ( s, ss )
                                }
                        , cache = doc.cache
                      }
                    , Cmd.none
                    )

                _ ->
                    ( { model | data = Error "Invalid data" }
                    , Cmd.none
                    )

        GotTechnikyDokument (Err error) ->
            ( { model | data = Error error }
            , Cmd.none
            )



--VIEW


view : Model -> Html Msg
view model =
    Html.div
        [ Attrs.css
            [ UI.normal
            , Css.fontFamily Css.sansSerif
            ]
        ]
        [ viewBody model
        ]


viewBody : Model -> Html Msg
viewBody model =
    case model.data of
        Data data ->
            Html.div
                []
                [ viewTechniky model.cache data
                ]

        Error message ->
            Html.text ("ERROR: " ++ message)

        Loading ->
            Html.text "Loading..."


viewTechniky : DatabaseCache -> DataPayload -> Html Msg
viewTechniky cache data =
    Html.div
        []
        [ Utils.cons data.stromy
            |> List.concatMap (viewBasic cache)
            |> Jutsu.viewTableContainer
        , Utils.cons data.quirky
            |> List.filter (not << QuirkMechanicky.neniVeHre)
            |> GUID.listSortBy QuirkMechanicky.id
            |> List.concatMap (viewSpecial cache data.techniky)
            |> Jutsu.viewTableContainer
        ]


viewSpecial : DatabaseCache -> List Jutsu -> QuirkMechanicky -> List (Html Msg)
viewSpecial cache allJutsu quirk =
    let
        quirkId : GUID.GUID GUID.QuirkMechanicky
        quirkId =
            QuirkMechanicky.id quirk
    in
    allJutsu
        |> List.filter (Jutsu.access >> JutsuAccess.isQuirk)
        |> List.filter (\j -> JutsuAccess.canAccess (GUID.setFromList [ quirkId ]) (Jutsu.access j))
        |> List.sortBy Jutsu.level
        |> List.map (Jutsu.viewTableRow cache Nothing)
        |> ifNotEmpty ((++) (Jutsu.viewTableHeader cache Nothing quirkId Nothing))


viewBasic : DatabaseCache -> Tree -> List (Html Msg)
viewBasic cache tree =
    let
        treeId : GUID.GUID GUID.Tree
        treeId =
            Tree.id tree
    in
    Tree.techniky tree
        |> List.filter (Jutsu.access >> JutsuAccess.isPublic)
        |> List.sortBy Jutsu.level
        |> List.map (Jutsu.viewTableRow cache (Tree.color tree))
        |> ifNotEmpty ((++) (Jutsu.viewTableHeader cache (Tree.color tree) treeId (Tree.pecet tree)))


ifNotEmpty : (List a -> List a) -> List a -> List a
ifNotEmpty fn data =
    if List.length data > 0 then
        fn data

    else
        []
