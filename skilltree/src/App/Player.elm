module App.Player exposing (main)

import Browser
import Character exposing (Character)
import Css
import DatabaseCache exposing (DatabaseCache)
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import Json.Decode
import Jutsu
import Platform.Cmd as Cmd
import QuirkMechanicky exposing (QuirkMechanicky)
import Tree exposing (Tree)
import UI
import UI.Columns
import Usecase.CharacterStats
import Usecase.ExtractHelp
import Usecase.TechnikyDocument exposing (TechnikyDocument)
import Utils exposing (ListNotEmpty)


main : Program Json.Decode.Value Model Msg
main =
    Browser.element
        { init = init
        , view = Html.toUnstyled << view
        , update = update
        , subscriptions = always Sub.none
        }



-- FLAGS


type alias Flags =
    { character : DatabaseCache -> Character
    }


flagsDecoder : Json.Decode.Decoder Flags
flagsDecoder =
    Json.Decode.map Flags
        (Json.Decode.field "character" Character.decoderSandbox)


decodeFlags : Json.Decode.Value -> Result Json.Decode.Error Flags
decodeFlags flags =
    Json.Decode.decodeValue flagsDecoder flags



-- INIT / MODEL


init : Json.Decode.Value -> ( Model, Cmd Msg )
init flagsRaw =
    ( { data =
            Loading
      }
    , Usecase.TechnikyDocument.fetch
        (GotTechnikyDocument (decodeFlags flagsRaw))
    )


type alias Model =
    { data : Data
    }


type Data
    = Loading
    | Error String
    | Data DataPayload


type alias DataPayload =
    { trees : ListNotEmpty Tree
    , quirks : List QuirkMechanicky
    , cache : DatabaseCache
    , character : Character
    }



-- UPDATE


type Msg
    = GotTechnikyDocument (Result Json.Decode.Error Flags) (Result String TechnikyDocument)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotTechnikyDocument (Ok flags) (Ok document) ->
            case document.stromy of
                head :: rest ->
                    ( { model
                        | data =
                            Data
                                { trees = ( head, rest )
                                , quirks = document.quirky
                                , cache = document.cache
                                , character = flags.character document.cache
                                }
                      }
                    , Cmd.none
                    )

                [] ->
                    ( { model | data = Error "Nebyly načteny žádné stromy" }
                    , Cmd.none
                    )

        GotTechnikyDocument _ (Err e) ->
            ( { model | data = Error e }
            , Cmd.none
            )

        GotTechnikyDocument (Err e) _ ->
            ( { model | data = Error (Json.Decode.errorToString e) }
            , Cmd.none
            )



-- VIEW


view : Model -> Html Msg
view model =
    Html.div
        [ Attrs.css
            [ UI.normal
            ]
        ]
        [ UI.Columns.view (viewBody model)
        ]


viewBody : Model -> List (Html Msg)
viewBody model =
    case model.data of
        Data data ->
            viewMain data

        Loading ->
            [ Html.text "Loading..."
            ]

        Error error ->
            [ Html.text ("Error: " ++ error)
            ]


viewMain : DataPayload -> List (Html msg)
viewMain data =
    [ Html.div
        []
        [ viewHelp data
        , UI.space (Css.rem 1)
        , Html.div
            [ Attrs.css
                [ Css.marginBottom (Css.rem 1)
                ]
            ]
            [ Html.h2
                [ Attrs.css
                    [ Css.margin (Css.rem 0)
                    ]
                ]
                [ Html.text "Zvolené techniky"
                ]
            , Usecase.CharacterStats.viewCharacterStats data.cache data.trees data.character
            ]
        , Tree.viewPickedList data.cache data.character data.trees
        ]
    ]


viewHelp : DataPayload -> Html msg
viewHelp data =
    Utils.cons data.trees
        |> List.concatMap Tree.techniky
        |> List.filter (Jutsu.id >> Character.hasTechnika data.character)
        |> Usecase.ExtractHelp.view data.cache
