module App.Help exposing (Data, Model, Msg, main)

import Browser
import Css
import DatabaseCache exposing (DatabaseCache)
import GUID
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import Jutsu exposing (Jutsu)
import JutsuAccess exposing (JutsuAccess)
import QuirkMechanicky exposing (QuirkMechanicky)
import Tree exposing (Tree)
import UI
import Usecase.TechnikyDocument
import Utils exposing (ListNotEmpty)
import Usecase.ExtractHelp


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , view = Html.toUnstyled << view
        , update = update
        , subscriptions = always Sub.none
        }


type alias Model =
    { cache : DatabaseCache
    , data : Data
    }


type Data
    = Loading
    | Error String
    | Data DataPayload


type alias DataPayload =
    { techniky : List Jutsu
    }



-- INIT


init : () -> ( Model, Cmd Msg )
init _ =
    ( { cache = DatabaseCache.empty
      , data = Loading
      }
    , Usecase.TechnikyDocument.fetch GotTechnikyDokument
    )



-- UPDATE


type Msg
    = GotTechnikyDokument (Result String Usecase.TechnikyDocument.TechnikyDocument)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotTechnikyDokument (Ok doc) ->
            case ( doc.quirky, doc.stromy ) of
                ( q :: qs, s :: ss ) ->
                    ( { model
                        | data =
                            Data
                                { techniky = List.concatMap Tree.techniky doc.stromy
                                }
                        , cache = doc.cache
                      }
                    , Cmd.none
                    )

                _ ->
                    ( { model | data = Error "Invalid data" }
                    , Cmd.none
                    )

        GotTechnikyDokument (Err error) ->
            ( { model | data = Error error }
            , Cmd.none
            )



--VIEW


view : Model -> Html Msg
view model =
    Html.div
        [ Attrs.css
            [ UI.normal
            , Css.fontFamily Css.sansSerif
            ]
        ]
        [ viewBody model
        ]


viewBody : Model -> Html Msg
viewBody model =
    case model.data of
        Data data ->
            Html.div
                []
                [ viewTechniky model.cache data
                ]

        Error message ->
            Html.text ("ERROR: " ++ message)

        Loading ->
            Html.text "Loading..."


viewTechniky : DatabaseCache -> DataPayload -> Html Msg
viewTechniky cache data =
    Html.div
        [ Attrs.css
            [ UI.textLittle
            ]
        ]
        [ Usecase.ExtractHelp.view cache data.techniky
        ]
