port module App.List exposing (Data, Model, Msg, main)

import Browser
import Css
import DatabaseCache exposing (DatabaseCache)
import GUID
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import Json.Decode
import Json.Encode
import Jutsu exposing (Jutsu)
import QuirkMechanicky exposing (QuirkMechanicky)
import Tree exposing (Tree)
import UI
import Usecase.JutsuFilter
import Usecase.TechnikyDocument
import Utils exposing (ListNotEmpty)


main : Program Json.Decode.Value Model Msg
main =
    Browser.element
        { init = init
        , view = Html.toUnstyled << view
        , update = update
        , subscriptions =
            \model ->
                case model.data of
                    Data data ->
                        routeReceiverSub model.cache data

                    _ ->
                        Sub.none
        }


type alias Model =
    { cache : DatabaseCache
    , data : Data
    }


type Data
    = Loading Flags
    | Error String
    | Data DataPayload


type alias DataPayload =
    { techniky : List Jutsu
    , quirky : ListNotEmpty QuirkMechanicky
    , stromy : ListNotEmpty Tree
    , filter : Usecase.JutsuFilter.State
    }



-- FLAGS / PORTS


port sendRoute : Json.Decode.Value -> Cmd msg


port routeReceiver : (Json.Decode.Value -> msg) -> Sub msg


port sendDarkTheme : Bool -> Cmd msg


routeReceiverSub : DatabaseCache -> DataPayload -> Sub Msg
routeReceiverSub cache data =
    let
        wrap : Result er (List String) -> Msg
        wrap filterResult =
            case Result.map (Usecase.JutsuFilter.fromRoute cache) filterResult of
                Ok (Just filter) ->
                    ChangedFilterFromOutside (Just { data | filter = filter })

                _ ->
                    ChangedFilterFromOutside Nothing
    in
    routeReceiver
        (Json.Decode.decodeValue routeDecoder >> wrap)


routeDecoder : Json.Decode.Decoder (List String)
routeDecoder =
    Json.Decode.field "route" Json.Decode.string
        |> Json.Decode.map (String.split "_")


encodeRoute : List String -> Json.Encode.Value
encodeRoute route =
    Json.Encode.object
        [ ( "route", Json.Encode.string (String.join "_" route) )
        ]


type alias Flags =
    { route : List String
    , darkTheme : Maybe Bool
    }


defaultFlags : Flags
defaultFlags =
    { route = []
    , darkTheme = Nothing
    }


flagsDecoder : Json.Decode.Decoder Flags
flagsDecoder =
    Json.Decode.map2 Flags
        routeDecoder
        (Json.Decode.maybe (Json.Decode.field "darkTheme" Json.Decode.bool))


decodeFlags : Json.Decode.Value -> Flags
decodeFlags flags =
    Json.Decode.decodeValue flagsDecoder flags
        -- |> Result.mapError (Debug.log "flags decode error")
        |> Result.withDefault defaultFlags



-- INIT


init : Json.Decode.Value -> ( Model, Cmd Msg )
init rawFlags =
    ( { cache = DatabaseCache.empty
      , data = Loading (decodeFlags rawFlags)
      }
    , Usecase.TechnikyDocument.fetch GotTechnikyDokument
    )



-- UPDATE


type Msg
    = ChangedFilter DataPayload
    | ChangedFilterFromOutside (Maybe DataPayload)
    | GotTechnikyDokument (Result String Usecase.TechnikyDocument.TechnikyDocument)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChangedFilter data ->
            ( { model | data = Data data }
            , sendRoute (encodeRoute (Usecase.JutsuFilter.toRoute data.filter))
            )

        ChangedFilterFromOutside (Just data) ->
            ( { model | data = Data data }
            , Cmd.none
            )

        ChangedFilterFromOutside Nothing ->
            ( model
            , Cmd.none
            )

        GotTechnikyDokument (Ok doc) ->
            case ( doc.quirky, doc.stromy ) of
                ( q :: qs, s :: ss ) ->
                    let
                        filter : Usecase.JutsuFilter.State
                        filter =
                            Maybe.withDefault (Usecase.JutsuFilter.init (Tree.id s)) <|
                                case model.data of
                                    Loading flags ->
                                        Usecase.JutsuFilter.fromRoute doc.cache flags.route

                                    _ ->
                                        Nothing
                    in
                    ( { model
                        | data =
                            Data
                                { techniky = List.sortBy Jutsu.level (List.concatMap Tree.techniky doc.stromy)
                                , quirky = ( q, qs )
                                , stromy = ( s, ss )
                                , filter = filter
                                }
                        , cache = doc.cache
                      }
                    , Cmd.none
                    )

                _ ->
                    ( { model | data = Error "Invalid data" }
                    , Cmd.none
                    )

        GotTechnikyDokument (Err error) ->
            ( { model | data = Error error }
            , Cmd.none
            )



--VIEW


view : Model -> Html Msg
view model =
    Html.div
        [ Attrs.css
            [ UI.normal
            , Css.fontFamily Css.sansSerif
            ]
        ]
        [ viewBody model
        ]


viewBody : Model -> Html Msg
viewBody model =
    case model.data of
        Data data ->
            Html.div
                []
                [ viewFilter model.cache data
                , viewHeader model.cache data.filter
                , viewTechniky model.cache data
                ]

        Error message ->
            Html.text ("ERROR: " ++ message)

        Loading _ ->
            Html.text "Loading..."


viewHeader : DatabaseCache -> Usecase.JutsuFilter.State -> Html Msg
viewHeader cache filter =
    Html.div
        [ Attrs.css
            [ Css.padding (Css.rem 0.5)
            ]
        ]
        [ Usecase.JutsuFilter.viewHeader cache filter
        ]


viewFilter : DatabaseCache -> DataPayload -> Html Msg
viewFilter cache data =
    Html.div
        [ Attrs.css
            [ UI.normal
            , Css.padding (Css.rem 0.5)
            , Css.position Css.sticky
            , Css.top Css.zero
            , Css.zIndex (Css.int 1)
            , Css.border3 (Css.px 1) Css.solid Css.transparent
            , Css.property "border-bottom-color" UI.varColMinorFg
            ]
        ]
        [ Usecase.JutsuFilter.view
            cache
            { onChange = \filter -> ChangedFilter { data | filter = filter }
            , quirks = data.quirky
            , trees = data.stromy
            }
            data.filter
        ]


viewTechniky : DatabaseCache -> DataPayload -> Html Msg
viewTechniky cache data =
    let
        filtered : List Jutsu
        filtered =
            data.techniky
                |> List.filter (Usecase.JutsuFilter.matches data.filter)
    in
    if List.length filtered == 0 then
        Html.div
            [ Attrs.css
                [ Css.padding (Css.rem 0.5)
                ]
            ]
            [ UI.minor "Žádné výsledky"
            ]

    else
        Html.div
            [ Attrs.css
                [ Css.padding (Css.rem 0.5)
                , Css.displayFlex
                , Css.flexDirection Css.row
                , Css.flexWrap Css.wrap
                , Css.alignItems Css.flexStart
                , Css.property "gap" "1rem"
                ]
            ]
            (filtered
                |> List.map (Jutsu.viewDetail cache "" True GUID.setEmpty)
            )
