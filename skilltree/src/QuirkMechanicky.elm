module QuirkMechanicky exposing
    ( QuirkMechanicky
    , compareTabLabel
    , decoder
    , id
    , isPublic
    , name
    , neniVeHre
    , tabLabelVerejne_TODO
    , tabLabel_TODO
    , updateCache
    , viewDetail
    )

import Css
import DatabaseCache exposing (DatabaseCache)
import GUID exposing (GUID, GUIDSet)
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import Utils exposing (ListNotEmpty)
import Xml.Decode


type QuirkMechanicky
    = QuirkMechanicky QuirkMechanickyPayload


type alias QuirkMechanickyPayload =
    { id : GUID GUID.QuirkMechanicky
    , name : String
    , playstyle : String
    , element : GUIDSet GUID.Element
    , kekkeiGenkai : Bool
    , spojujeStromy : GUIDSet GUID.Tree
    , neniVeHre : Bool
    , access : Access
    }


id : QuirkMechanicky -> GUID GUID.QuirkMechanicky
id (QuirkMechanicky quirk) =
    quirk.id


name : QuirkMechanicky -> String
name (QuirkMechanicky quirk) =
    quirk.name


neniVeHre : QuirkMechanicky -> Bool
neniVeHre (QuirkMechanicky quirk) =
    quirk.neniVeHre


tabLabelVerejne_TODO : String
tabLabelVerejne_TODO =
    "Veřejné"


tabLabelTajne_TODO : String
tabLabelTajne_TODO =
    "Tajné"


tabLabel_TODO : DatabaseCache -> QuirkMechanicky -> ListNotEmpty String
tabLabel_TODO cache (QuirkMechanicky quirk) =
    case quirk.access of
        Public ->
            ( tabLabelVerejne_TODO, [] )

        Village ids ->
            List.filterMap (DatabaseCache.name cache) ids
                |> Utils.listNotEmpty
                |> Maybe.withDefault ( "", [] )

        Secret ->
            ( tabLabelTajne_TODO, [] )


compareTabLabel : String -> String -> Order
compareTabLabel a b =
    if a == b then
        EQ

    else if a == tabLabelVerejne_TODO || b == tabLabelTajne_TODO then
        LT

    else if b == tabLabelVerejne_TODO || a == tabLabelTajne_TODO then
        GT

    else
        compare a b


isPublic : QuirkMechanicky -> Bool
isPublic (QuirkMechanicky quirk) =
    case quirk.access of
        Public ->
            True

        _ ->
            False



-- DATABASE CACHE


updateCache : QuirkMechanicky -> DatabaseCache -> DatabaseCache
updateCache (QuirkMechanicky quirk) cache =
    cache
        |> DatabaseCache.registerQuirk quirk.id
            { element = quirk.element
            , kekkeiGenkai = quirk.kekkeiGenkai
            , spojujeStromy = quirk.spojujeStromy
            }
            { name = quirk.name
            , details = ""
            , alias = Nothing
            }



-- DECODER


maybeFallback : a -> Xml.Decode.Decoder a -> Xml.Decode.Decoder a
maybeFallback fallback =
    Xml.Decode.maybe >> Xml.Decode.map (Maybe.withDefault fallback)


decoder : Xml.Decode.Decoder QuirkMechanicky
decoder =
    Xml.Decode.succeed QuirkMechanickyPayload
        |> Xml.Decode.andMap GUID.idDecoder
        |> Xml.Decode.andMap (Xml.Decode.path [ "name" ] (Xml.Decode.single Xml.Decode.string))
        |> Xml.Decode.andMap (Xml.Decode.path [ "playstyle" ] (Xml.Decode.single Xml.Decode.string))
        |> Xml.Decode.andMap (Xml.Decode.map GUID.setFromList <| GUID.refListPathDecoder [ "element" ])
        |> Xml.Decode.andMap
            (Xml.Decode.boolAttr "kekkei-genkai"
                |> Xml.Decode.maybe
                |> Xml.Decode.map (Maybe.withDefault False)
            )
        |> Xml.Decode.andMap (Xml.Decode.map GUID.setFromList <| GUID.refListPathDecoder [ "spojuje", "strom" ])
        |> Xml.Decode.andMap (maybeFallback False (Xml.Decode.boolAttr "není-ve-hře"))
        |> Xml.Decode.andMap accessDecoder
        |> Xml.Decode.map QuirkMechanicky



-- ACCESS


type Access
    = Village (List (GUID GUID.Common))
    | Public
    | Secret


accessDecoder : Xml.Decode.Decoder Access
accessDecoder =
    Xml.Decode.oneOf
        [ Xml.Decode.path [ "public" ] (Xml.Decode.single (Xml.Decode.succeed Public))
        , Xml.Decode.path [ "secret" ] (Xml.Decode.single (Xml.Decode.succeed Secret))
        , Xml.Decode.path [ "village" ] (Xml.Decode.list GUID.refDecoder)
            |> Xml.Decode.map Village
        ]



-- VIEW / DETAIL


viewDetail : QuirkMechanicky -> Html msg
viewDetail (QuirkMechanicky quirk) =
    Html.div
        [ Attrs.css
            [ Css.padding (Css.rem 0.5)
            , Css.display Css.none
            ]
        ]
        [ Html.text quirk.playstyle
        ]
