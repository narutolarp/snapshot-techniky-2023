module UI.TreeGrid exposing (Options, TreePayload, view)

import Css
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import UI.Heading


type alias TreePayload item =
    { name : String
    , altName : Maybe String
    , intro : String
    , image : Maybe String
    , jutsuByLevel : List (List item)
    }


type GridRow item
    = JutsuRow (List item)
    | TreeHeader String (Maybe String) String (Maybe String)


transposeJutsu : List (List item) -> List (List item)
transposeJutsu jutsuByLevel =
    if List.length jutsuByLevel > 0 then
        List.filterMap List.head jutsuByLevel
            :: transposeJutsu (List.filterMap List.tail jutsuByLevel)

    else
        []


convertToRows : List (TreePayload item) -> List (GridRow item)
convertToRows source =
    case source of
        tree :: rest ->
            TreeHeader tree.name tree.altName tree.intro tree.image
                :: List.map JutsuRow (transposeJutsu tree.jutsuByLevel)
                ++ convertToRows rest

        [] ->
            []



-- VIEW


type alias Options item msg =
    { viewItem : item -> Html msg
    , column : item -> Int
    }


view : Options item msg -> List (TreePayload item) -> Html msg
view options data =
    Html.div
        [ Attrs.css
            [ Css.property "display" "grid"
            , Css.property "gap" "0.5rem"
            , Css.overflowX Css.auto
            , Css.padding (Css.rem 0.5)
            ]
        ]
        (List.indexedMap (viewRow options) (convertToRows data))


viewRow : Options item msg -> Int -> GridRow item -> Html msg
viewRow options rowIndex row =
    case row of
        JutsuRow jutsu ->
            wrap (viewJutsuRow options rowIndex jutsu)

        TreeHeader name altName intro image ->
            viewTreeHeader name altName intro image rowIndex


viewTreeHeader : String -> Maybe String -> String -> Maybe String -> Int -> Html msg
viewTreeHeader name altName intro image rowIndex =
    UI.Heading.view
        { level = Html.h3
        , heading = name
        , alias = altName
        , subtitle = Just intro
        , image = image
        }
        [ Css.property "grid-row" (String.fromInt (rowIndex + 1))
        , Css.property "grid-column" "1 / span 6"
        , Css.marginTop (Css.rem 0.5)
        , Css.marginBottom Css.zero
        , Css.maxWidth (Css.calc (Css.vw 100) Css.minus (Css.rem 1))
        , Css.position Css.sticky
        , Css.left Css.zero
        ]


viewJutsuRow : Options item msg -> Int -> List item -> List (Html msg)
viewJutsuRow options rowIndex row =
    List.map (viewItem options rowIndex) row


viewItem : Options item msg -> Int -> item -> Html msg
viewItem options rowIndex item =
    Html.div
        [ Attrs.css
            [ Css.property "grid-row" (String.fromInt (rowIndex + 1))
            , Css.property "grid-column" (String.fromInt (options.column item))
            ]
        ]
        [ options.viewItem item
        ]



-- UTIL


wrap : List (Html msg) -> Html msg
wrap =
    Html.div
        [ Attrs.css
            [ Css.property "display" "contents"
            ]
        ]
