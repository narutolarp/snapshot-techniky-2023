module UI.BlockQuote exposing (view)

import Css
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import UI


view : String -> Html msg
view text =
    Html.div
        [ Attrs.css
            [ Css.paddingLeft (Css.rem 0.3)
            , Css.property "border-left" ("0.2rem solid " ++ UI.varColMinorBg)
            ]
        ]
        [ UI.minor text
        ]
