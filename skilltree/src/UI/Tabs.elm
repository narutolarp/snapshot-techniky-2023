module UI.Tabs exposing (Option, OptionDisplay(..), basicStringOption, view)

import Css
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import Html.Styled.Events as Events
import UI


type OptionDisplay
    = Normal
    | Error


type alias Option a =
    { value : a
    , label : String
    , badge : Maybe String
    , display : OptionDisplay
    }


basicStringOption : String -> Option String
basicStringOption label =
    { value = label
    , label = label
    , badge = Nothing
    , display = Normal
    }



-- VIEW


view : (a -> a -> Bool) -> (a -> msg) -> List (Option a) -> a -> Html msg
view eql onSelect options selected =
    Html.div
        [ Attrs.css
            [ Css.displayFlex
            , Css.flexWrap Css.wrap
            , Css.property "gap" "1px"
            ]
        ]
        (List.map (viewTab eql onSelect selected) options)


viewTab : (a -> a -> Bool) -> (a -> msg) -> a -> Option a -> Html msg
viewTab eql onSelect selected option =
    Html.button
        [ Events.onClick (onSelect option.value)
        , Attrs.css
            [ buttonStyles option
            ]
        ]
        [ viewLabel (eql selected option.value) option.label
        , viewBadge option.badge
        ]


buttonStyles : Option a -> Css.Style
buttonStyles option =
    Css.batch <|
        case option.display of
            Normal ->
                []

            Error ->
                [ UI.error
                ]


viewLabel : Bool -> String -> Html msg
viewLabel selected label =
    Html.span
        [ Attrs.css
            [ Css.fontWeight Css.bold
            , selectedButtonLabelStyles selected
            ]
        ]
        [ Html.text label
        ]


viewBadge : Maybe String -> Html msg
viewBadge badge =
    case badge of
        Just text ->
            Html.span
                [ Attrs.css
                    [ UI.textMinor
                    , Css.position Css.relative
                    , Css.left (Css.rem 0.2)
                    , Css.bottom (Css.rem 0.2)
                    ]
                ]
                [ Html.text text
                ]

        Nothing ->
            Html.text ""


selectedButtonLabelStyles : Bool -> Css.Style
selectedButtonLabelStyles isSelected =
    Css.batch <|
        if isSelected then
            [ Css.textDecoration Css.underline
            ]

        else
            [ UI.textGray
            ]
