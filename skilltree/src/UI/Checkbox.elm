module UI.Checkbox exposing (Editability(..), Options, view, viewStylable)

import Css
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import Html.Styled.Events as Events
import UI


type alias Options msg =
    { label : String
    , onChange : Bool -> msg
    , value : Bool
    , editability : Editability
    }


type Editability
    = Editable
    | Disabled
    | Locked
    | PermaLocked


view : Options msg -> Html msg
view options =
    viewStylable options []


viewStylable : Options msg -> UI.StylableElement msg
viewStylable options styles =
    let
        locked : Bool
        locked =
            options.editability == Locked || options.editability == PermaLocked

        inputDisabled : Bool
        inputDisabled =
            locked && not options.value || options.editability == Disabled
    in
    Html.label
        [ Attrs.css
            [ Css.batch styles
            , UI.whenCss locked
                UI.textGray
            , UI.whenCss (locked && options.value) <|
                Css.batch
                    [ UI.error
                    , Css.property "box-shadow" ("0 0 0.2rem 0.2rem " ++ UI.varColBg)
                    ]
            , Css.displayFlex
            , Css.property "gap" "0.2rem"
            ]
        , UI.whenAttr inputDisabled <|
            Attrs.tabindex 0
        ]
        [ Html.input
            [ Attrs.type_ "checkbox"
            , Attrs.checked options.value
            , Attrs.disabled inputDisabled
            , Events.onCheck options.onChange
            , Attrs.css
                [ Css.margin Css.zero
                , Css.width (Css.rem 1)
                , Css.height (Css.rem 1)
                , Css.flexShrink Css.zero
                , UI.whenCss (options.editability == PermaLocked) <|
                    Css.display Css.none
                ]
            ]
            []
        , UI.whenHtml (options.editability == PermaLocked) <|
            Html.span
                [ Attrs.css
                    [ Css.width (Css.rem 1)
                    , Css.height (Css.rem 1)
                    , Css.displayFlex
                    , Css.alignItems Css.center
                    ]
                ]
                [ Html.text "🔒"
                ]
        , Html.text options.label
        ]
