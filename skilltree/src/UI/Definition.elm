module UI.Definition exposing (indentBlock, join, joinNames, listLabeled, multiply, stringLabeled, stringLabeledWithAlias, stringListLabeled, viewLabel, viewName, viewSimpleBlock)

import Css
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import List.Extra
import UI



--INLINE


viewLabel : String -> Html msg
viewLabel label =
    Html.span
        [ Attrs.css
            [ UI.textMinor
            ]
        ]
        [ Html.text (label ++ ": ")
        ]


viewName : String -> Html msg
viewName label =
    Html.strong
        []
        [ Html.text label
        ]


multiply : ( Html msg, Int ) -> Html msg
multiply ( name, count ) =
    let
        countText : String
        countText =
            if count > 1 then
                String.fromInt count ++ "x "

            else
                ""
    in
    Html.span
        []
        [ Html.text countText
        , name
        ]


addSpacesToSeparator : String -> String
addSpacesToSeparator sep =
    case String.trim sep of
        "," ->
            ", "

        _ ->
            " " ++ sep ++ " "


joinNames : String -> List String -> List String -> Html msg
joinNames sep names suffix =
    join sep (List.map viewName (names ++ suffix))


join : String -> List (Html msg) -> Html msg
join sep list =
    case List.Extra.unconsLast list of
        Just ( last, first :: body ) ->
            Html.span
                []
                (List.intersperse (UI.gray ", ") (first :: body)
                    ++ [ UI.gray (addSpacesToSeparator sep)
                       , last
                       ]
                )

        Just ( last, [] ) ->
            Html.span
                []
                [ last ]

        Nothing ->
            Html.text ""



--ONELINE


stringLabeled : String -> Maybe String -> Html msg
stringLabeled label value =
    stringLabeledWithAlias label value Nothing


stringLabeledWithAlias : String -> Maybe String -> Maybe String -> Html msg
stringLabeledWithAlias label value alias_ =
    case value of
        Just val ->
            Html.div
                []
                [ viewLabel label
                , viewName val
                , UI.gray <|
                    case alias_ of
                        Just als ->
                            " (" ++ als ++ ")"

                        Nothing ->
                            ""
                ]

        Nothing ->
            Html.text ""


stringListLabeled : String -> String -> List String -> Html msg
stringListLabeled sep label values =
    listLabeled sep label (List.map viewName values)


listLabeled : String -> String -> List (Html msg) -> Html msg
listLabeled sep label values =
    if List.length values > 0 then
        Html.div
            []
            [ viewLabel label
            , join sep values
            ]

    else
        Html.text ""



-- BLOCK


indentBlock : Css.Style
indentBlock =
    Css.paddingLeft (Css.rem 1)


viewSimpleBlock : String -> List (Html msg) -> Html msg
viewSimpleBlock label items =
    if List.length items == 0 then
        Html.text ""

    else
        Html.div
            [ Attrs.css
                [ Css.displayFlex
                , Css.flexDirection Css.column
                , Css.property "gap" "3px"
                ]
            ]
            [ UI.little label
            , Html.div
                [ Attrs.css
                    [ Css.displayFlex
                    , Css.flexDirection Css.column
                    , indentBlock
                    , Css.property "gap" "3px"
                    ]
                ]
                items
            ]
