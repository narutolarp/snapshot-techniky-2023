module UI.SimpleCheckboxGrid exposing (Options, view)

import Css
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import Html.Styled.Lazy
import UI
import UI.Checkbox
import UI.Tooltip


type alias Options item msg =
    { viewLabel : item -> String
    , viewDetail : item -> Html msg
    , isSelected : item -> Bool
    , isFullWidth : item -> Bool
    , editable : Bool
    , onChange : item -> Bool -> msg
    }



-- VIEW


view : UI.Tooltip.Position -> Options item msg -> List item -> Html msg
view tooltipPosition opts items =
    Html.div
        [ Attrs.css
            [ Css.property "display" "grid"
            , Css.property "grid-template-columns" "repeat(auto-fit, 12rem)"
            , Css.property "gap" "0.2rem"
            ]
        ]
        (List.map (viewItem tooltipPosition opts) items)


viewItem : UI.Tooltip.Position -> Options item msg -> item -> Html msg
viewItem tooltipPosition opts item =
    Html.div
        [ Attrs.css
            [ UI.whenCss (opts.isFullWidth item) <|
                Css.property "grid-column" "1 / -1"
            ]
        ]
        [ UI.Tooltip.view
            { position = tooltipPosition
            , width = Css.rem 20
            , above = False
            }
            (UI.Checkbox.viewStylable
                { label = opts.viewLabel item
                , value = opts.isSelected item
                , editability = itemEditability opts
                , onChange = opts.onChange item
                }
            )
            (Html.Styled.Lazy.lazy opts.viewDetail item)
        ]


itemEditability : Options item msg -> UI.Checkbox.Editability
itemEditability options =
    if options.editable then
        UI.Checkbox.Editable

    else
        UI.Checkbox.Disabled
