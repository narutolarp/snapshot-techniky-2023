module UI.Tooltip exposing (Position(..), TooltipOptions, view)

import Css
import Css.Global
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import UI


type alias TooltipOptions compatible =
    { width : Css.LengthOrAuto compatible
    , position : Position
    , above : Bool
    }


view : TooltipOptions a -> UI.StylableElement msg -> Html msg -> Html msg
view options visiblePart tooltip =
    let
        targetTooltip : List Css.Style -> Css.Style
        targetTooltip style =
            Css.Global.children
                [ Css.Global.class "elm--tooltip" style
                ]

        showTooltip : List Css.Global.Snippet
        showTooltip =
            [ Css.Global.class "elm--tooltip" [ Css.displayFlex ]
            ]
    in
    Html.div
        [ Attrs.css
            [ UI.whenCss (options.position /= Full) <|
                Css.position Css.relative
            , targetTooltip
                [ Css.display Css.none
                ]
            , Css.hover
                [ targetTooltip
                    [ Css.displayFlex
                    ]
                ]
            ]
        ]
        [ visiblePart
            [ Css.pseudoClass "focus-visible"
                [ Css.Global.adjacentSiblings
                    showTooltip
                ]
            , Css.pseudoClass "has(:focus-visible)"
                [ Css.Global.adjacentSiblings
                    showTooltip
                ]
            ]
        , Html.div
            [ Attrs.class "elm--tooltip"
            , Attrs.css
                [ Css.position Css.absolute
                , Css.borderRadius (Css.rem 0.2)
                , Css.width options.width
                , Css.maxWidth (Css.vw 100)
                , tooltipPosition options.position options.above
                , Css.zIndex (Css.int 1)
                , Css.pointerEvents Css.none
                , UI.normal
                ]
            ]
            [ tooltip
            ]
        ]


type Position
    = Left
    | Right
    | Center
    | Full


tooltipPosition : Position -> Bool -> Css.Style
tooltipPosition position above =
    Css.batch
        [ tooltipPositionHorizontal position
        , tooltipPositionVertical above
        ]


tooltipPositionVertical : Bool -> Css.Style
tooltipPositionVertical above =
    if above then
        Css.batch
            [ Css.bottom (Css.calc (Css.pct 100) Css.minus (Css.px 1))
            , Css.boxShadow4 (Css.rem 0.1) (Css.rem -0.5) (Css.rem 1) (Css.hex "#000")
            ]

    else
        Css.batch
            [ Css.top (Css.calc (Css.pct 100) Css.minus (Css.px 1))
            , Css.boxShadow4 (Css.rem 0.1) (Css.rem 0.5) (Css.rem 1) (Css.hex "#000")
            ]


tooltipPositionHorizontal : Position -> Css.Style
tooltipPositionHorizontal position =
    case position of
        Left ->
            Css.right Css.zero

        Right ->
            Css.left Css.zero

        Center ->
            Css.batch
                [ Css.left (Css.pct 50)
                , Css.transform (Css.translateX (Css.pct -50))
                ]

        Full ->
            Css.batch
                [ Css.left (Css.rem 1)
                , Css.right (Css.rem 1)
                , Css.top Css.unset
                , Css.width Css.unset
                ]
