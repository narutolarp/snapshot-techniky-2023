module UI.GridWithDialog exposing (Props, PropsTabs, view, DialogMode(..))

import Css
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import List.Extra
import UI
import UI.Dialog
import UI.Heading
import UI.SimpleCheckboxGrid
import UI.Tabs
import UI.Tooltip
import Utils exposing (ListNotEmpty)


type alias PropsTabs item msg =
    { label : item -> ListNotEmpty String
    , selected : String
    , onSelect : String -> msg
    }


type alias Props item msg =
    { title : String
    , dialogMode : DialogMode item msg
    }


type DialogMode item msg
    = Openable
        { openLabel : String
        , tabProps : Maybe (PropsTabs item msg)
        }
    | NonOpenable


view : Props item msg -> UI.SimpleCheckboxGrid.Options item msg -> List item -> Html msg
view props gridOptions items =
    Html.div
        []
        [ UI.Heading.view
            { level = Html.h4
            , heading = props.title
            , alias = Nothing
            , subtitle = Nothing
            , image = Nothing
            }
            []
        , UI.SimpleCheckboxGrid.view
            UI.Tooltip.Right
            gridOptions
            (List.filter gridOptions.isSelected items)
        , case props.dialogMode of
            Openable { openLabel, tabProps } ->
                Html.div
                    []
                    [ UI.space (Css.rem 0.2)
                    , UI.Dialog.view
                        openLabel
                        (viewDialogContent props.title tabProps gridOptions items)
                    ]

            NonOpenable ->
                Html.text ""
        ]


viewDialogContent : String -> Maybe (PropsTabs item msg) -> UI.SimpleCheckboxGrid.Options item msg -> List item -> List (Html msg)
viewDialogContent title tabProps gridOptions items =
    [ UI.Heading.view
        { level = Html.h3
        , heading = title
        , alias = Nothing
        , subtitle = Nothing
        , image = Nothing
        }
        []
    , case tabProps of
        Just tabs ->
            let
                tabOptions : List (UI.Tabs.Option String)
                tabOptions =
                    List.concatMap (tabs.label >> Utils.cons) items
                        |> List.Extra.unique
                        |> List.map UI.Tabs.basicStringOption
            in
            Html.div
                [ Attrs.css
                    [ Css.marginBottom (Css.rem 0.5)
                    ]
                ]
                [ UI.Tabs.view (==) tabs.onSelect tabOptions tabs.selected
                ]

        Nothing ->
            Html.text ""
    , UI.SimpleCheckboxGrid.view
        UI.Tooltip.Full
        gridOptions
        (case tabProps of
            Just tabs ->
                List.filter (tabs.label >> Utils.cons >> List.Extra.find ((==) tabs.selected) >> (/=) Nothing) items

            Nothing ->
                items
        )
    ]
