module UI.List exposing (view)

import Css
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs


view : List Css.Style -> List (Html msg) -> Html msg
view styles items =
    Html.div
        [ Attrs.css
            [ Css.displayFlex
            , Css.flexDirection Css.column
            , Css.property "gap" "1rem"
            , Css.batch styles
            ]
        ]
        items
