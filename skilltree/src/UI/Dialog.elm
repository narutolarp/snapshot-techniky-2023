module UI.Dialog exposing (view)

import Css
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import Json.Encode
import UI


view : String -> List (Html msg) -> Html msg
view openLabel content =
    Html.div
        []
        [ buttonOpen openLabel
        , Html.node "dialog"
            [ Attrs.css
                [ Css.width (Css.rem 41)
                , Css.minHeight (Css.vh 60)
                , UI.normal
                ]
            ]
            (content ++ [ UI.space (Css.rem 7), buttonClose ])
        ]


buttonOpen : String -> Html msg
buttonOpen openLabel =
    Html.node "dialog-control-button"
        [ Attrs.property "btnlabel" (Json.Encode.string openLabel)
        , Attrs.property "btnclass" (Json.Encode.string "minor")
        , Attrs.attribute "btnaction" "open"
        ]
        []


buttonClose : Html msg
buttonClose =
    Html.node "dialog-control-button"
        [ Attrs.property "btnlabel" (Json.Encode.string "Ok")
        , Attrs.property "btnclass" (Json.Encode.string "primary")
        , Attrs.attribute "btnaction" "close"
        , Attrs.css
            [ Css.display Css.block
            , Css.position Css.sticky
            , Css.bottom Css.zero
            ]
        ]
        []
