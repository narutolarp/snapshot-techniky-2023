module Usecase.ExtractHelp exposing (view)

import Css
import DatabaseCache exposing (DatabaseCache)
import GUID
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import Html.Styled.Events as Events
import Jutsu exposing (Jutsu)
import UI.Definition


view : DatabaseCache -> List Jutsu -> Html msg
view cache jutsu =
    List.foldl (GUID.setUnion << Jutsu.extractTerms) GUID.setEmpty jutsu
        |> GUID.setToList
        |> List.filter (DatabaseCache.isDefined cache)
        |> List.sortBy (DatabaseCache.name cache >> Maybe.withDefault "")
        |> List.map (DatabaseCache.viewDefinition cache)
        |> UI.Definition.viewSimpleBlock "Nápověda"
