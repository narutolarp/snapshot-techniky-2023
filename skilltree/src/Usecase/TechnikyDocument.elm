module Usecase.TechnikyDocument exposing (TechnikyDocument, fetch)

import Api
import DatabaseCache exposing (DatabaseCache)
import Env.Env
import GUID exposing (GUID)
import QuirkMechanicky exposing (QuirkMechanicky)
import Tree exposing (Tree)
import Xml.Decode


type alias TechnikyDocument =
    { stromy : List Tree
    , quirky : List QuirkMechanicky
    , cache : DatabaseCache
    }


definicePaths =
    [ ( "zásah-definice", "zásah", DatabaseCache.commonDecoder )
    , ( "heslo-definice", "ofenzivní", DatabaseCache.commonDecoder )
    , ( "heslo-definice", "defenzivní", DatabaseCache.commonDecoder )
    , ( "atribut-definice", "atribut", DatabaseCache.atributDecoder )
    , ( "cena-definice", "cena", DatabaseCache.commonDecoder )
    , ( "cooldown-definice", "cooldown", DatabaseCache.commonDecoder )
    , ( "typ-vnímání-definice", "typ-vnímání", DatabaseCache.commonDecoder )
    , ( "klíčové-slovo-definice", "klíčové-slovo", DatabaseCache.commonDecoder )
    , ( "typ-efektu-definice", "typ-efektu", DatabaseCache.commonDecoder )
    , ( "modifikátor-efektu-definice", "modifikátor-efektu", DatabaseCache.commonDecoder )
    , ( "element-definice", "element", DatabaseCache.elementDecoder )

    -- , ( "pečeť-definice", "pečeť" ) TODO <pečeť id="pes" obrázek="dog" />
    ]


technikyDocumentDecoder : Xml.Decode.Decoder TechnikyDocument
technikyDocumentDecoder =
    Xml.Decode.with (Xml.Decode.path [ "strom" ] (Xml.Decode.list Tree.decoder)) <|
        \stromy ->
            Xml.Decode.with (Xml.Decode.path [ "quirk-list-mechanicky", "quirk-mechanicky" ] (Xml.Decode.list QuirkMechanicky.decoder)) <|
                \quirky ->
                    Xml.Decode.with (cacheDecoder definicePaths (initCache stromy quirky)) <|
                        \cache ->
                            Xml.Decode.succeed (TechnikyDocument stromy quirky cache)


fetch : (Result String TechnikyDocument -> msg) -> Cmd msg
fetch msg =
    Api.fetch
        { url = Env.Env.databasePublicUrlBase ++ "techniky.xml?v=5"
        , decoder = technikyDocumentDecoder
        , msg = msg
        }



-- CACHE


initCache : List Tree -> List QuirkMechanicky -> DatabaseCache
initCache stromy quirky =
    DatabaseCache.empty
        |> DatabaseCache.registerIterHelper Tree.updateCache stromy
        |> DatabaseCache.registerIterHelper QuirkMechanicky.updateCache quirky
        |> DatabaseCache.registerIterHelper updateCacheVillage_hardcoded GUID.const.villages


updateCacheVillage_hardcoded : GUID GUID.Village -> DatabaseCache -> DatabaseCache
updateCacheVillage_hardcoded id =
    DatabaseCache.registerCommon id
        { name = GUID.villageName_TODO id
        , details = ""
        , alias = Nothing
        }


cacheDecoder tagNames cache =
    case tagNames of
        ( tag, childTag, decoder ) :: rest ->
            Xml.Decode.with
                (Xml.Decode.path [ tag ] (Xml.Decode.single (decoder childTag cache)))
                (\c -> cacheDecoder rest c)

        [] ->
            Xml.Decode.succeed cache
