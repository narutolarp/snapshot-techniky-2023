module Usecase.JutsuFilter exposing (Props, State, fromRoute, init, matches, toRoute, view, viewHeader)

import Css
import DatabaseCache exposing (DatabaseCache)
import GUID exposing (GUID)
import Html.Styled as Html exposing (Html)
import Jutsu exposing (Jutsu)
import JutsuAccess
import List.Extra
import QuirkMechanicky exposing (QuirkMechanicky)
import Tree exposing (Tree)
import UI
import UI.Tabs
import Utils exposing (ListNotEmpty)


type State
    = State StatePayload


type alias StatePayload =
    { filter : Filter
    }


type Filter
    = Public (GUID GUID.Tree)
    | Quirk String (GUID GUID.QuirkMechanicky)


init : GUID GUID.Tree -> State
init id =
    State { filter = Public id }


topLevelEqual : Filter -> Filter -> Bool
topLevelEqual a b =
    case ( a, b ) of
        ( Public _, Public _ ) ->
            True

        ( Quirk _ _, Quirk _ _ ) ->
            True

        _ ->
            False


fromRoute : DatabaseCache -> List String -> Maybe State
fromRoute cache route =
    Maybe.map (State << StatePayload) <|
        case route of
            [ "public", treeId ] ->
                DatabaseCache.stringValidateTree cache treeId
                    |> Maybe.map Public

            [ "quirk", quirkTab, quirkId ] ->
                DatabaseCache.stringValidateSpecialniSchopnost cache quirkId
                    |> Maybe.map (Quirk quirkTab)

            _ ->
                Nothing


toRoute : State -> List String
toRoute (State state) =
    case state.filter of
        Public treeId ->
            [ "public", GUID.toString_HACK treeId ]

        Quirk quirkTab quirkId ->
            [ "quirk", quirkTab, GUID.toString_HACK quirkId ]



-- FILTER


matches : State -> Jutsu -> Bool
matches (State state) jutsu =
    case state.filter of
        Public tree ->
            GUID.equal (Jutsu.tree jutsu) tree
                && JutsuAccess.isPublic (Jutsu.access jutsu)

        Quirk _ quirk ->
            JutsuAccess.canAccess (GUID.setFromList [ quirk ]) (Jutsu.access jutsu)
                && JutsuAccess.isQuirk (Jutsu.access jutsu)



-- VIEW CURRENT FILTER HEADER


viewHeader : DatabaseCache -> State -> Html msg
viewHeader cache (State state) =
    case state.filter of
        Public tree ->
            DatabaseCache.viewCommonBlock cache tree

        Quirk _ quirk ->
            DatabaseCache.viewCommonBlock cache quirk



-- VIEW


type alias Props msg =
    { quirks : ListNotEmpty QuirkMechanicky
    , trees : ListNotEmpty Tree
    , onChange : State -> msg
    }


view : DatabaseCache -> Props msg -> State -> Html msg
view cache props (State state) =
    let
        onFilter : Filter -> msg
        onFilter new =
            props.onChange (State { state | filter = new })
    in
    Html.div
        []
        [ UI.Tabs.view topLevelEqual onFilter (topLevelTabs (Tuple.first props.trees) props.quirks) state.filter
        , UI.space (Css.rem 0.5)
        , viewSecondLevel cache onFilter state.filter (Utils.cons props.trees) props.quirks
        ]


viewSecondLevel : DatabaseCache -> (Filter -> msg) -> Filter -> List Tree -> ListNotEmpty QuirkMechanicky -> Html msg
viewSecondLevel cache onFilter filter trees quirks =
    case filter of
        Public tree ->
            UI.Tabs.view GUID.equal (Public >> onFilter) (List.map optionFromTree trees) tree

        Quirk quirkTab quirk ->
            let
                quirkTabs : QuirkMechanicky -> List String
                quirkTabs =
                    QuirkMechanicky.tabLabel_TODO cache >> Utils.cons

                quirkAccessTabs : List (UI.Tabs.Option String)
                quirkAccessTabs =
                    List.concatMap quirkTabs (Utils.cons quirks)
                        |> List.Extra.unique
                        |> List.sortWith QuirkMechanicky.compareTabLabel
                        |> List.map UI.Tabs.basicStringOption

                setAccessTab : String -> Filter
                setAccessTab tab =
                    Quirk tab (QuirkMechanicky.id (Utils.alwaysFind (isMatching tab) quirks))

                isMatching : String -> QuirkMechanicky -> Bool
                isMatching tab quir =
                    quirkTabs quir
                        |> List.Extra.find ((==) tab)
                        |> (/=) Nothing

                matchingQuirks : List QuirkMechanicky
                matchingQuirks =
                    List.filter (isMatching quirkTab) (Utils.cons quirks)
            in
            Html.div
                []
                [ UI.Tabs.view (==) (setAccessTab >> onFilter) quirkAccessTabs quirkTab
                , UI.space (Css.rem 0.5)
                , UI.Tabs.view GUID.equal (Quirk quirkTab >> onFilter) (List.map optionFromQuirk matchingQuirks) quirk
                ]


optionFromTree : Tree -> UI.Tabs.Option (GUID GUID.Tree)
optionFromTree tree =
    { value = Tree.id tree
    , label = Tree.name tree
    , badge = Nothing
    , display = UI.Tabs.Normal
    }


optionFromQuirk : QuirkMechanicky -> UI.Tabs.Option (GUID GUID.QuirkMechanicky)
optionFromQuirk quirk =
    { value = QuirkMechanicky.id quirk
    , label = QuirkMechanicky.name quirk
    , badge = Nothing
    , display = UI.Tabs.Normal
    }


topLevelTab : Filter -> String -> UI.Tabs.Option Filter
topLevelTab filter label =
    { value = filter
    , label = label
    , badge = Nothing
    , display = UI.Tabs.Normal
    }


topLevelTabs : Tree -> ListNotEmpty QuirkMechanicky -> List (UI.Tabs.Option Filter)
topLevelTabs tree quirky =
    [ topLevelTab (Public (Tree.id tree)) "Základní schopnosti"
    , topLevelTab
        (Quirk QuirkMechanicky.tabLabelVerejne_TODO (QuirkMechanicky.id (Utils.alwaysFind QuirkMechanicky.isPublic quirky)))
        "Speciální schopnosti"
    ]
