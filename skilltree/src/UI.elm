module UI exposing
    ( StylableElement
    , borderNormal
    , borderNormalWidth
    , col
    , error
    , gray
    , little
    , minor
    , normal
    , row
    , space
    , textGray
    , textLittle
    , textMinor
    , varColBg
    , varColFg
    , varColMinorBg
    , varColMinorFg
    , whenAttr
    , whenCss
    , whenHtml
    )

import Css
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs


type alias StylableElement msg =
    List Css.Style -> Html msg


ifElse : Bool -> a -> a -> a
ifElse predicate main fallback =
    if predicate then
        main

    else
        fallback


whenCss : Bool -> Css.Style -> Css.Style
whenCss predicate style =
    ifElse predicate style (Css.batch [])


whenHtml : Bool -> Html msg -> Html msg
whenHtml predicate element =
    ifElse predicate element (Html.text "")


whenAttr : Bool -> Html.Attribute msg -> Html.Attribute msg
whenAttr predicate attr =
    ifElse predicate attr (Attrs.class "")



-- elements


minor : String -> Html msg
minor txt =
    Html.span
        [ Attrs.css [ textMinor ] ]
        [ Html.text txt ]


gray : String -> Html msg
gray txt =
    Html.span
        [ Attrs.css [ textGray ] ]
        [ Html.text txt ]


little : String -> Html msg
little txt =
    Html.span
        [ Attrs.css [ textLittle ] ]
        [ Html.text txt ]



-- common styles


type alias VarCol =
    { bg : String
    , minorBg : String
    , fg : String
    , minorFg : String
    }


varColNames : VarCol
varColNames =
    { bg = "--col--bg"
    , minorBg = "--col--minor-bg"
    , fg = "--col--fg"
    , minorFg = "--col--minor-fg"
    }


varColNormal : VarCol
varColNormal =
    { bg = "var(--col-normal--bg, #fff)"
    , minorBg = "var(--col-normal--minor-bg, #ddd)"
    , fg = "var(--col-normal--fg, #000)"
    , minorFg = "var(--col-normal--minor-fg, #767676)"
    }


varColError : VarCol
varColError =
    { bg = "var(--col-error--bg, #faa)"
    , minorBg = "var(--col-normal--minor-bg, #fcc)"
    , fg = "var(--col-error--fg, #000)"
    , minorFg = "var(--col-error--minor-fg, #767676)"
    }


var : String -> String -> Css.Style
var prop varName =
    Css.property prop ("var(" ++ varName ++ ")")


assignVars : VarCol -> Css.Style
assignVars vars =
    Css.batch
        [ Css.property varColNames.bg vars.bg
        , Css.property varColNames.minorBg vars.minorBg
        , Css.property varColNames.fg vars.fg
        , Css.property varColNames.minorFg vars.minorFg
        , var "color" varColNames.fg
        , var "background-color" varColNames.bg
        ]


varColBg : String
varColBg =
    "var(" ++ varColNames.bg ++ ")"


varColMinorBg : String
varColMinorBg =
    "var(" ++ varColNames.minorBg ++ ")"


varColFg : String
varColFg =
    "var(" ++ varColNames.fg ++ ")"


varColMinorFg : String
varColMinorFg =
    "var(" ++ varColNames.minorFg ++ ")"


normal : Css.Style
normal =
    assignVars varColNormal


error : Css.Style
error =
    assignVars varColError


textGray : Css.Style
textGray =
    var "color" varColNames.minorFg


textMinor : Css.Style
textMinor =
    Css.batch
        [ textGray
        , textLittle
        ]


textLittle : Css.Style
textLittle =
    Css.fontSize (Css.rem 0.8)


borderNormalWidth : String
borderNormalWidth =
    "1px"


borderNormal : Css.Style
borderNormal =
    Css.property "border" (borderNormalWidth ++ " solid " ++ varColFg)



-- layout


row : List (Html.Attribute msg) -> List (Html msg) -> Html msg
row =
    Html.styled Html.div
        [ Css.displayFlex
        , Css.property "gap" "0.5rem"
        ]


col : List (Html.Attribute msg) -> List (Html msg) -> Html msg
col =
    Html.styled Html.div
        [ Css.displayFlex
        , Css.flexDirection Css.column
        , Css.property "gap" "1rem"
        ]


space : Css.LengthOrAuto compatible -> Html msg
space height =
    Html.div
        [ Attrs.css
            [ Css.height height
            ]
        ]
        []
