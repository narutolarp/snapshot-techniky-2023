module Tree exposing
    ( SkilltreeOptions
    , Tree
    , allJutsuPicked
    , color
    , decoder
    , id
    , name
    , neschopnosti
    , pecet
    , techniky
    , updateCache
    , viewPickedList
    , viewSkilltree
    )

import Character exposing (Character)
import DatabaseCache exposing (DatabaseCache)
import GUID exposing (GUID, GUIDSet)
import Html.Styled as Html exposing (Html)
import Jutsu exposing (Jutsu)
import JutsuAccess
import List.Extra
import UI.Heading
import UI.List
import UI.TreeGrid
import Utils exposing (ListNotEmpty)
import Xml.Decode


type Tree
    = Tree TreePayload


type alias TreePayload =
    { id : GUID GUID.Tree
    , color : Maybe String
    , name : String
    , nameCZ : Maybe String
    , playstyle : String
    , flavour : String
    , pecet : Maybe (GUID GUID.Common)
    , element : Maybe (GUID GUID.Element)
    , techniky : List Jutsu
    }


id : Tree -> GUID GUID.Tree
id (Tree tree) =
    tree.id


color : Tree -> Maybe String
color (Tree tree) =
    tree.color


name : Tree -> String
name (Tree tree) =
    tree.nameCZ
        |> Maybe.withDefault tree.name


altName : Tree -> Maybe String
altName (Tree tree) =
    tree.nameCZ
        |> Maybe.map (always tree.name)


element : Tree -> Maybe (GUID GUID.Element)
element (Tree tree) =
    tree.element


pecet : Tree -> Maybe (GUID GUID.Common)
pecet (Tree tree) =
    tree.pecet


pecetImage : Tree -> Maybe String
pecetImage tree =
    Maybe.map GUID.pecetToImgUrl (pecet tree)


techniky : Tree -> List Jutsu
techniky (Tree tree) =
    tree.techniky


treeOfJutsu : ListNotEmpty Tree -> Jutsu -> Tree
treeOfJutsu trees jutsu =
    Utils.alwaysFind (id >> GUID.equal (Jutsu.tree jutsu)) trees


neschopnosti : ListNotEmpty Tree -> List (GUID GUID.Neschopnost)
neschopnosti trees =
    GUID.const.neschopnostElementy
        :: (Utils.cons trees
                |> List.filter (element >> (==) Nothing)
                |> List.map (id >> GUID.toNeschopnost)
           )


technikyAccesible : Character -> Tree -> List Jutsu
technikyAccesible character (Tree tree) =
    List.filter (canAccess character) tree.techniky


canAccess : Character -> Jutsu -> Bool
canAccess character jutsu =
    JutsuAccess.canAccess (Character.quirksMechanicke character) (Jutsu.access jutsu)


allJutsuPicked : Character -> ListNotEmpty Tree -> List Jutsu
allJutsuPicked character trees =
    List.filter (Jutsu.id >> Character.hasTechnika character) (allJutsu character trees)


allJutsu : Character -> ListNotEmpty Tree -> List Jutsu
allJutsu character trees =
    List.concatMap (technikyAccesible character) (Utils.cons trees)


isJutsuAllowed : Character -> ListNotEmpty Tree -> List Section -> Jutsu -> Bool
isJutsuAllowed char trees allSections jutsu =
    let
        isAllowIter : List Section -> Bool
        isAllowIter sections =
            case sections of
                head :: rest ->
                    hasPrevLevel char head jutsu
                        && isPyramidOK char trees jutsu
                        || isAllowIter rest

                [] ->
                    False
    in
    Jutsu.level jutsu <= 1 || isAllowIter (sectionOfJutsu allSections jutsu)


hasPrevLevel : Character -> Section -> Jutsu -> Bool
hasPrevLevel char section jutsu =
    Nothing
        /= List.Extra.find
            (\j -> Jutsu.level j == Jutsu.level jutsu - 1 && Character.hasTechnika char (Jutsu.id j))
            (technikyAccesibleSection char section)


isPyramidOK : Character -> ListNotEmpty Tree -> Jutsu -> Bool
isPyramidOK char trees jutsu =
    let
        characterJutsuLevels : List Int
        characterJutsuLevels =
            allJutsu char trees
                |> List.filter (Jutsu.id >> Character.hasTechnika char)
                |> List.map Jutsu.level

        prevLevelCount : Int
        prevLevelCount =
            characterJutsuLevels
                |> List.filter ((==) (Jutsu.level jutsu - 1))
                |> List.length

        thisLevelCount : Int
        thisLevelCount =
            characterJutsuLevels
                |> List.filter ((==) (Jutsu.level jutsu))
                |> List.length
    in
    if Character.hasTechnika char (Jutsu.id jutsu) then
        prevLevelCount > thisLevelCount

    else
        prevLevelCount > thisLevelCount + 1



-- SECTION (connects trees together for kekkei-genkai)


type alias Section =
    ( Maybe (GUID GUID.QuirkMechanicky), ListNotEmpty Tree )


jutsuIdsInSection : Section -> GUIDSet GUID.Jutsu
jutsuIdsInSection ( _, trees ) =
    Utils.cons trees
        |> List.concatMap techniky
        |> List.map Jutsu.id
        |> GUID.setFromList


sectionOfJutsu : List Section -> Jutsu -> List Section
sectionOfJutsu sections jutsu =
    List.filter
        (jutsuIdsInSection >> (\set -> GUID.setHas set (Jutsu.id jutsu)))
        sections


technikySection : Section -> List Jutsu
technikySection ( _, trees ) =
    Utils.cons trees
        |> List.concatMap techniky


technikyAccesibleSection : Character -> Section -> List Jutsu
technikyAccesibleSection char section =
    List.filter (canAccess char) (technikySection section)


groupTreesByQuirks : DatabaseCache -> Character -> ListNotEmpty Tree -> List Section
groupTreesByQuirks cache char trees =
    let
        getQuirkPayloadWithId : GUID GUID.QuirkMechanicky -> Maybe ( GUID GUID.QuirkMechanicky, DatabaseCache.QuirkPayload )
        getQuirkPayloadWithId quirkId =
            DatabaseCache.quirkPayload cache quirkId
                |> Maybe.map (\quirk -> ( quirkId, quirk ))

        sectionsToMake : List ( GUID GUID.QuirkMechanicky, GUIDSet GUID.Tree )
        sectionsToMake =
            Character.quirksMechanicke char
                |> GUID.setToList
                |> List.filterMap getQuirkPayloadWithId
                |> List.map (Tuple.mapSecond .spojujeStromy)
                |> List.filter (Tuple.second >> GUID.setLength >> (/=) 0)

        treesInAnySection : GUIDSet GUID.Tree
        treesInAnySection =
            sectionsToMake
                |> List.map Tuple.second
                |> List.foldl GUID.setUnion GUID.setEmpty

        normalTrees : List Tree
        normalTrees =
            List.filter (not << GUID.setHas treesInAnySection << id) (Utils.cons trees)

        makeSection : ( GUID GUID.QuirkMechanicky, GUIDSet GUID.Tree ) -> Maybe Section
        makeSection ( quirkId, treeIds ) =
            Utils.cons trees
                |> List.filter (id >> GUID.setHas treeIds)
                |> Utils.listNotEmpty
                |> Maybe.map (\t -> ( Just quirkId, t ))
    in
    List.filterMap makeSection sectionsToMake
        ++ List.map (\tree -> ( Nothing, ( tree, [] ) )) normalTrees



-- XML decoder


decoder : Xml.Decode.Decoder Tree
decoder =
    Xml.Decode.map Tree payloadDecoder


payloadDecoder : Xml.Decode.Decoder TreePayload
payloadDecoder =
    Xml.Decode.with GUID.idDecoder <|
        \id_ ->
            Xml.Decode.succeed (TreePayload id_)
                |> Xml.Decode.andMap (Xml.Decode.maybe (Xml.Decode.stringAttr "barva"))
                |> Xml.Decode.requiredPath [ "jméno" ] (Xml.Decode.single Xml.Decode.string)
                |> Xml.Decode.possiblePath [ "jméno-cz" ] (Xml.Decode.single Xml.Decode.string)
                |> Xml.Decode.requiredPath [ "styl-hry" ] (Xml.Decode.single Xml.Decode.string)
                |> Xml.Decode.requiredPath [ "flavour" ] (Xml.Decode.single Xml.Decode.string)
                |> Xml.Decode.possiblePath [ "pečeť" ] (Xml.Decode.single GUID.refDecoder)
                |> Xml.Decode.possiblePath [ "element" ] (Xml.Decode.single GUID.refDecoder)
                |> Xml.Decode.requiredPath [ "technika" ] (Xml.Decode.list (Jutsu.decoder id_))



-- CACHE


updateCache : Tree -> DatabaseCache -> DatabaseCache
updateCache (Tree tree) cache =
    cache
        |> DatabaseCache.registerTree tree.id
            {}
            { name = name (Tree tree)
            , details = tree.flavour
            , alias = altName (Tree tree)
            }
        |> DatabaseCache.registerIterHelper Jutsu.updateCache tree.techniky



-- VIEW TABS
-- viewTabs : (GUID GUID.Tree -> msg) -> Character -> ListNotEmpty Tree -> GUID GUID.Tree -> Html msg
-- viewTabs msg character trees activeTree =
--     let
--         accessibleTrees =
--             List.filter (technikyAccesible character >> List.length >> (/=) 0) (cons trees)
--     in
--     UI.Tabs.view msg (List.map (stromToOption trees character) accessibleTrees) activeTree
-- stromToOption : ListNotEmpty Tree -> Character -> Tree -> UI.Tabs.Option GUID.Tree
-- stromToOption allTrees character strom =
--     { value = id strom
--     , label = name strom
--     , badge =
--         case List.length (technikyPickedFromTree character strom) of
--             0 ->
--                 Nothing
--             len ->
--                 Just (String.fromInt len)
--     , display =
--         if List.all (isJutsuAllowed character allTrees) (technikyPickedFromTree character strom) then
--             UI.Tabs.Normal
--         else
--             UI.Tabs.Error
--     }
--
--
--
-- VIEW LIST


viewPickedList : DatabaseCache -> Character -> ListNotEmpty Tree -> Html msg
viewPickedList cache character trees =
    UI.List.view
        []
        (groupTreesByQuirks cache character trees
            |> List.map (viewPickedListSection cache character)
        )


viewPickedListSection : DatabaseCache -> Character -> Section -> Html msg
viewPickedListSection cache char ( quirkId, relevantTrees ) =
    case relevantTrees of
        ( Tree tree, [] ) ->
            viewPickedSingleTree cache char Html.h3 (Tree tree)

        ( _, _ :: _ ) ->
            Html.div
                []
                (UI.Heading.view
                    { level = Html.h3
                    , heading = Maybe.andThen (DatabaseCache.name cache) quirkId |> Maybe.withDefault ""
                    , alias = Maybe.andThen (DatabaseCache.nameAlias cache) quirkId
                    , subtitle =
                        DatabaseCache.viewLabeledListShortString "a" "Spojuje stromy" cache (List.map id (Utils.cons relevantTrees))
                            |> Just
                    , image = Nothing
                    }
                    []
                    :: List.map (viewPickedSingleTree cache char Html.h4) (Utils.cons relevantTrees)
                )


viewPickedSingleTree :
    DatabaseCache
    -> Character
    -> (List (Html.Attribute msg) -> List (Html msg) -> Html msg)
    -> Tree
    -> Html msg
viewPickedSingleTree cache char level tree =
    let
        picked : List Jutsu
        picked =
            allJutsuPicked char ( tree, [] )
                |> List.sortBy Jutsu.level
    in
    if List.length picked == 0 then
        Html.text ""

    else
        UI.List.view
            []
            (UI.Heading.view
                { level = level
                , heading = name tree
                , alias = altName tree
                , subtitle = Nothing
                , image = pecetImage tree
                }
                []
                :: List.map
                    (Jutsu.viewDetail cache "" False (Character.quirksMechanicke char))
                    picked
            )



-- VIEW INFO
-- VIEW SKILLTREE


type alias SkilltreeOptions msg =
    { character : Character
    , onCharacter : Character -> msg
    }


viewSkilltree : DatabaseCache -> SkilltreeOptions msg -> ListNotEmpty Tree -> Html msg
viewSkilltree cache options trees =
    let
        sections : List Section
        sections =
            groupTreesByQuirks cache options.character trees
    in
    UI.TreeGrid.view
        { viewItem = viewSingleCheckbox cache options trees sections
        , column = Jutsu.level
        }
        (List.filterMap (sectionToGridPayload cache options.character) sections)


sectionToGridPayload : DatabaseCache -> Character -> Section -> Maybe (UI.TreeGrid.TreePayload Jutsu)
sectionToGridPayload cache char ( quirkId, relevantTrees ) =
    case relevantTrees of
        ( Tree tree, [] ) ->
            let
                accessible : List Jutsu
                accessible =
                    technikyAccesible char (Tree tree)
            in
            if List.length accessible > 0 then
                Just
                    { name = name (Tree tree)
                    , altName = altName (Tree tree)
                    , intro = tree.flavour
                    , image = pecetImage (Tree tree)
                    , jutsuByLevel = Jutsu.groupByLevel accessible
                    }

            else
                Nothing

        ( _, _ :: _ ) ->
            let
                accessible : List Jutsu
                accessible =
                    List.concatMap (technikyAccesible char) (Utils.cons relevantTrees)
            in
            if List.length accessible > 0 then
                Just
                    { name = Maybe.andThen (DatabaseCache.name cache) quirkId |> Maybe.withDefault ""
                    , altName = Maybe.andThen (DatabaseCache.nameAlias cache) quirkId
                    , intro = DatabaseCache.viewLabeledListShortString "a" "Spojuje stromy" cache (List.map id (Utils.cons relevantTrees))
                    , jutsuByLevel = Jutsu.groupByLevel accessible
                    , image = Nothing
                    }

            else
                Nothing


viewSingleCheckbox : DatabaseCache -> SkilltreeOptions msg -> ListNotEmpty Tree -> List Section -> Jutsu -> Html msg
viewSingleCheckbox cache options trees sections jutsu =
    let
        isAllowed : Bool
        isAllowed =
            isJutsuAllowed options.character trees sections jutsu

        volneXP : Int
        volneXP =
            Character.zbyvajiciXP options.character

        isSelected : Bool
        isSelected =
            Character.hasTechnika options.character (Jutsu.id jutsu)

        jutsuElement : Maybe (GUID GUID.Element)
        jutsuElement =
            element (treeOfJutsu trees jutsu)
    in
    Jutsu.viewCheckbox
        cache
        { selected = isSelected
        , locked = not (isAllowed && (isSelected || volneXP > 0) && volneXP >= 0)
        , onChange = options.onCharacter << Character.setTechnika (Jutsu.id jutsu) options.character
        , permaLock =
            if lacksElement cache options.character jutsuElement then
                Jutsu.Element

            else if Jutsu.level jutsu > 1 && hasNeschopnost jutsuElement jutsu options.character then
                Jutsu.Neschopnost

            else if Character.level options.character + 1 < Jutsu.level jutsu then
                Jutsu.CharacterLevel (Jutsu.level jutsu - (Character.level options.character + 1))

            else
                Jutsu.None
        , relevantQuirks =
            Character.quirksMechanicke options.character
        , isInUpperHalf =
            Utils.cons trees
                |> List.Extra.findIndex (GUID.equal (Jutsu.tree jutsu) << id)
                |> Maybe.withDefault -1
                |> (>) (List.length (Utils.cons trees) // 2)
        }
        jutsu


lacksElement : DatabaseCache -> Character -> Maybe (GUID GUID.Element) -> Bool
lacksElement cache char jutsuElement =
    case jutsuElement of
        Just elem ->
            not (Character.hasElement cache char elem)

        Nothing ->
            -- if there is no element, then character cannot lack if (i.e. there is no issue)
            False


hasNeschopnost : Maybe (GUID GUID.Element) -> Jutsu -> Character -> Bool
hasNeschopnost jutsuElement jutsu character =
    Character.hasNeschopnost character (Jutsu.tree jutsu)
        || (Character.hasNeschopnost character GUID.const.neschopnostElementy
                && (jutsuElement /= Nothing)
           )
