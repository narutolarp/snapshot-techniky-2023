module JutsuEffect exposing
    ( Atribut
    , Effect
    , SlevaPayload
    , effectsDecoder
    , extractTerms
    , givenAttributes
    , givenSleva
    , isPassive
    , separateInlinePassiveActive
    , view
    , viewShort
    , viewSlevaShort
    )

import Css
import DatabaseCache exposing (DatabaseCache)
import GUID exposing (GUID, GUIDSet)
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import UI
import UI.Definition
import Utils exposing (Vsechny(..))
import Xml.Decode


givenAttributes : Effect -> List Atribut
givenAttributes (Effect _ inner) =
    case inner of
        Pasivni attrs _ ->
            attrs

        _ ->
            []


givenSleva : Effect -> Maybe SlevaPayload
givenSleva (Effect _ inner) =
    case inner of
        Pasivni _ sleva ->
            sleva

        _ ->
            Nothing


separateInlinePassiveActive : List Effect -> ( List Effect, List Effect, List Effect )
separateInlinePassiveActive effects =
    let
        trulyPassive : List Effect
        trulyPassive =
            List.filter
                isPassive
                effects

        other : List Effect
        other =
            List.filter
                (not << isPassive)
                effects
    in
    ( trulyPassive
    , []
    , other
    )


isPassive : Effect -> Bool
isPassive (Effect _ efekt) =
    case efekt of
        Pasivni _ _ ->
            True

        _ ->
            False


extractTerms : Effect -> GUIDSet GUID.Common
extractTerms (Effect payload effect) =
    GUID.setEmpty
        |> extractTermsShared payload
        |> extractTermsInner effect


extractTermsInner : InnerEffect -> GUIDSet GUID.Common -> GUIDSet GUID.Common
extractTermsInner effect =
    case effect of
        Pasivni _ sleva ->
            GUID.setUnion (GUID.setFromList (Maybe.map (.cena >> List.map Tuple.first) sleva |> Maybe.withDefault []))

        Kombo { attack, req } podminky ->
            extractZasah attack
                >> Maybe.withDefault identity (Maybe.map extractZasah req)
                >> GUID.setUnion (GUID.setFromList podminky.reqHesla)
                >> GUID.setAddIf GUID.const.followupEfekt (req /= Nothing)

        Utocna { attack, req } ->
            extractZasah attack
                >> Maybe.withDefault identity (Maybe.map extractZasah req)
                >> GUID.setAddIf GUID.const.followupEfekt (req /= Nothing)

        Obranna zasah stinZivot ->
            extractZasah zasah
                >> GUID.setAddIf GUID.const.stinZivot stinZivot

        Leceni _ ->
            GUID.setAdd GUID.const.leceni

        Neviditelnost _ ->
            GUID.setAdd GUID.const.skryti

        Vnimani _ ->
            GUID.setAdd GUID.const.vnimani

        Specialni ->
            identity


extractTermsShared : EffectPayload -> GUIDSet GUID.Common -> GUIDSet GUID.Common
extractTermsShared shared =
    GUID.setUnion (GUID.setFromList shared.efektNaSesilatele)


extractZasah : ZasahPayload -> GUIDSet GUID.Common -> GUIDSet GUID.Common
extractZasah zasah =
    GUID.setUnion (GUID.setFromList zasah.heslo)
        >> GUID.setUnion (GUID.setFromList zasah.zasah)



-- POMOCNE TYPY


type alias ZasahPayload =
    { zasah : List (GUID GUID.Common)
    , heslo : List (GUID GUID.Common)
    , dmg : Int
    }


payloadDecoderZasah : Xml.Decode.Decoder ZasahPayload
payloadDecoderZasah =
    Xml.Decode.map3 ZasahPayload
        (GUID.refListPathDecoder [ "zásah" ])
        (GUID.refListPathDecoder [ "heslo" ])
        (Xml.Decode.optionalPath [ "zranění" ] (Xml.Decode.single Xml.Decode.int) 0 (Xml.Decode.succeed identity))


type alias PodminkyPayload =
    { reqHesla : List (GUID GUID.Common)
    , reqDmg : Int
    }


payloadDecoderPodminky : Xml.Decode.Decoder PodminkyPayload
payloadDecoderPodminky =
    Xml.Decode.map2 PodminkyPayload
        (GUID.refListPathDecoder [ "podmínky", "heslo" ])
        (Xml.Decode.optionalPath [ "podmínky", "zranění" ] (Xml.Decode.single Xml.Decode.int) 0 (Xml.Decode.succeed identity))


type alias LeceniPayload =
    { hesla : Vsechny (List (GUID GUID.Common))
    , dmg : Vsechny Int
    }


payloadDecoderLeceni : Xml.Decode.Decoder LeceniPayload
payloadDecoderLeceni =
    Xml.Decode.map2 LeceniPayload
        (Xml.Decode.oneOf
            [ Xml.Decode.path [ "všechny-hesla" ] (Xml.Decode.single (Xml.Decode.succeed Vsechny))
            , GUID.refListPathDecoder [ "heslo" ]
                |> Xml.Decode.map Prave
            ]
        )
        (Xml.Decode.oneOf
            [ Xml.Decode.path [ "všechny-zranění" ] (Xml.Decode.single (Xml.Decode.succeed Vsechny))
            , Xml.Decode.optionalPath [ "zranění" ] (Xml.Decode.single Xml.Decode.int) 0 (Xml.Decode.succeed identity)
                |> Xml.Decode.map Prave
            ]
        )


type alias VnimaniPayload =
    { typ : GUID GUID.Common
    , lvl : Int
    }


payloadDecoderVnimani : Xml.Decode.Decoder VnimaniPayload
payloadDecoderVnimani =
    Xml.Decode.map2 VnimaniPayload
        (Xml.Decode.path [ "typ-vnímání" ] (Xml.Decode.single GUID.refDecoder))
        (Xml.Decode.path [ "úroveň" ] (Xml.Decode.single Xml.Decode.int))


type TechnikaSelector
    = SelectVsechnyTechniky
    | SelectStrom (GUIDSet GUID.Tree)
    | SelectTechnika (GUIDSet GUID.Jutsu)


type alias SlevaPayload =
    { selector : TechnikaSelector
    , cena : List ( GUID GUID.Common, Int )
    }


slevaDecoder : Xml.Decode.Decoder SlevaPayload
slevaDecoder =
    let
        selectorDecoder : Xml.Decode.Decoder TechnikaSelector
        selectorDecoder =
            Xml.Decode.oneOf
                [ Xml.Decode.path [ "sleva", "všechny-techniky" ] (Xml.Decode.single (Xml.Decode.succeed SelectVsechnyTechniky))
                , GUID.refListPathDecoderNonEmpty [ "sleva", "strom" ]
                    |> Xml.Decode.map (SelectStrom << GUID.setFromList)
                , GUID.refListPathDecoderNonEmpty [ "sleva", "technika" ]
                    |> Xml.Decode.map (SelectTechnika << GUID.setFromList)
                ]
    in
    Xml.Decode.map2 SlevaPayload
        selectorDecoder
        (Xml.Decode.path [ "sleva", "cena" ] (Xml.Decode.list GUID.refIntDecoder))



-- EFFECT


type alias EffectPayload =
    { specialni : String
    , vylepsujeTechniku : Maybe (GUID GUID.Jutsu)
    , efektNaSesilatele : List (GUID GUID.Common)
    , zdvojitCenu : Bool
    }


type Effect
    = Effect EffectPayload InnerEffect


type alias Atribut =
    ( GUID GUID.Atribut, Int )


type alias AttackPayload =
    { attack : ZasahPayload
    , req : Maybe ZasahPayload
    }


type InnerEffect
    = Pasivni (List Atribut) (Maybe SlevaPayload)
    | Kombo AttackPayload PodminkyPayload
    | Utocna AttackPayload
    | Obranna ZasahPayload Bool
    | Leceni LeceniPayload
    | Neviditelnost Int
    | Vnimani VnimaniPayload
    | Specialni


effectsWrapDecoder : Xml.Decode.Decoder InnerEffect -> Xml.Decode.Decoder Effect
effectsWrapDecoder =
    Xml.Decode.map2 Effect
        effectPayloadDecoder


effectPayloadDecoder : Xml.Decode.Decoder EffectPayload
effectPayloadDecoder =
    Xml.Decode.map4 EffectPayload
        (Xml.Decode.optionalPath [ "speciální" ] (Xml.Decode.single Xml.Decode.string) "" (Xml.Decode.succeed identity))
        (Xml.Decode.maybe <| Xml.Decode.path [ "vylepšuje-techniku" ] (Xml.Decode.single GUID.refDecoder))
        (GUID.refListPathDecoder [ "efekt-na-sesilatele" ])
        (Xml.Decode.optionalPath [ "zdvojit-cenu" ] (Xml.Decode.single (Xml.Decode.succeed True)) False (Xml.Decode.succeed identity))


effectsDecoder : Xml.Decode.Decoder (List Effect)
effectsDecoder =
    Xml.Decode.map2 List.append
        (Xml.Decode.path [ "pasivní" ] (Xml.Decode.list effectDecoderPasivni))
        (Xml.Decode.path [ "kombo" ] (Xml.Decode.list effectDecoderKombo))
        |> Xml.Decode.map2 List.append
            (Xml.Decode.path [ "útočná" ] (Xml.Decode.list effectDecoderUtocna))
        |> Xml.Decode.map2 List.append
            (Xml.Decode.path [ "obranná" ] (Xml.Decode.list effectDecoderObranna))
        |> Xml.Decode.map2 List.append
            (Xml.Decode.path [ "léčení" ] (Xml.Decode.list effectDecoderLeceni))
        |> Xml.Decode.map2 List.append
            (Xml.Decode.path [ "neviditelnost" ] (Xml.Decode.list effectDecoderNeviditelnost))
        |> Xml.Decode.map2 List.append
            (Xml.Decode.path [ "vnímání" ] (Xml.Decode.list efectDecoderVnimani))
        |> Xml.Decode.map2 List.append
            (Xml.Decode.path [ "speciální" ] (Xml.Decode.list (effectsWrapDecoder <| Xml.Decode.succeed Specialni)))


effectDecoderNeviditelnost : Xml.Decode.Decoder Effect
effectDecoderNeviditelnost =
    effectsWrapDecoder <|
        Xml.Decode.map Neviditelnost
            (Xml.Decode.path [ "úroveň" ] (Xml.Decode.single Xml.Decode.int))


efectDecoderVnimani : Xml.Decode.Decoder Effect
efectDecoderVnimani =
    effectsWrapDecoder <|
        Xml.Decode.map Vnimani
            payloadDecoderVnimani


effectDecoderPasivni : Xml.Decode.Decoder Effect
effectDecoderPasivni =
    effectsWrapDecoder <|
        Xml.Decode.map2 Pasivni
            (Xml.Decode.path [ "atribut" ] (Xml.Decode.list GUID.refIntDecoder))
            (Xml.Decode.maybe slevaDecoder)


effectDecoderKombo : Xml.Decode.Decoder Effect
effectDecoderKombo =
    effectsWrapDecoder <|
        Xml.Decode.map2 Kombo
            attackWithFollowupDecoder
            payloadDecoderPodminky


effectDecoderUtocna : Xml.Decode.Decoder Effect
effectDecoderUtocna =
    effectsWrapDecoder <|
        Xml.Decode.map Utocna <|
            attackWithFollowupDecoder


attackWithFollowupDecoder : Xml.Decode.Decoder AttackPayload
attackWithFollowupDecoder =
    Xml.Decode.map2 AttackPayload
        payloadDecoderZasah
        (Xml.Decode.possiblePath [ "musí-následovat-zásah" ]
            (Xml.Decode.single payloadDecoderZasah)
            (Xml.Decode.succeed identity)
        )


effectDecoderObranna : Xml.Decode.Decoder Effect
effectDecoderObranna =
    effectsWrapDecoder <|
        Xml.Decode.map2 Obranna
            payloadDecoderZasah
            (Xml.Decode.possiblePath [ "stínový-život" ]
                (Xml.Decode.single (Xml.Decode.succeed True))
                (Xml.Decode.succeed (Maybe.withDefault False))
            )


effectDecoderLeceni : Xml.Decode.Decoder Effect
effectDecoderLeceni =
    effectsWrapDecoder <|
        Xml.Decode.map Leceni
            payloadDecoderLeceni



-- VIEW


hasNormalEffect : InnerEffect -> Bool
hasNormalEffect effect =
    case effect of
        Utocna _ ->
            True

        Kombo _ _ ->
            True

        Obranna _ _ ->
            True

        _ ->
            False


viewShort : DatabaseCache -> Effect -> Html msg
viewShort =
    viewInner True


view : DatabaseCache -> Effect -> Html msg
view =
    viewInner False


viewInner : Bool -> DatabaseCache -> Effect -> Html msg
viewInner short cache (Effect shared effect) =
    let
        mainEffect : List (Html msg)
        mainEffect =
            [ viewMainEffect short cache effect
            , UI.space (Css.px 3)
            , viewEfektNaSesilatele cache shared.efektNaSesilatele
            , UI.whenHtml (shared.specialni /= "") <|
                UI.space (Css.px 3)
            , Html.text shared.specialni
            ]
    in
    if isPassive (Effect shared effect) then
        Html.div
            []
            mainEffect

    else
        viewEffectHeader
            short
            cache
            shared.zdvojitCenu
            (effectMod effect)
            (case ( shared.vylepsujeTechniku, hasNormalEffect effect ) of
                ( Just vylepsujeTechniku, True ) ->
                    [ DatabaseCache.viewLabeledListShort "" "Přidává nový efekt technice" cache [ vylepsujeTechniku ]
                    , indentEffect short cache shared.zdvojitCenu (effectMod effect) mainEffect
                    ]

                ( Just vylepsujeTechniku, False ) ->
                    DatabaseCache.viewLabeledListShort "" "Vylepšuje techniku" cache [ vylepsujeTechniku ]
                        :: mainEffect

                ( Nothing, _ ) ->
                    mainEffect
            )


effectMod : InnerEffect -> Maybe String
effectMod effect =
    case effect of
        Utocna { req } ->
            req
                |> Maybe.map (always "Kombo")

        Kombo { req } _ ->
            req
                |> Maybe.map (always "Kombo")

        _ ->
            Nothing


viewEfektNaSesilatele : DatabaseCache -> List (GUID GUID.Common) -> Html msg
viewEfektNaSesilatele cache efektNaSesilatele =
    if List.length efektNaSesilatele == 0 then
        Html.text ""

    else
        DatabaseCache.viewLabeledListShortCustom "," "Po provedení techniky sesilatel dostane" cache efektNaSesilatele [ "Automaticky" ]


viewEffectHeader : Bool -> DatabaseCache -> Bool -> Maybe String -> List (Html msg) -> Html msg
viewEffectHeader short _ zdvojitCenu effectModificator children =
    Html.div
        [ Attrs.css
            [ Css.property "border-top" ("1px solid " ++ UI.varColMinorFg)
            , Css.padding (Css.rem 0.2)
            , Css.paddingTop (Css.rem 0.6)
            , Css.marginTop (Css.rem 0.6)
            , Css.position Css.relative
            ]
            |> UI.whenAttr (not short)
        ]
        ((UI.whenHtml (not short) <|
            Html.span
                [ Attrs.css
                    [ Css.position Css.absolute
                    , Css.top (Css.rem -0.8)
                    , Css.left (Css.rem 1)
                    , Css.padding (Css.rem 0.2)
                    , UI.normal
                    , UI.textLittle
                    ]
                ]
                [ Html.text "Efekt"
                , case effectModificator of
                    Just mod ->
                        Html.text (" (" ++ mod ++ ")")

                    Nothing ->
                        Html.text ""
                ]
         )
            :: UI.whenHtml zdvojitCenu
                (Html.div [] [ Html.text "Tento efekt má dvojnásobnou sesílací cenu" ])
            :: children
        )


indentEffect : Bool -> DatabaseCache -> Bool -> Maybe String -> List (Html msg) -> Html msg
indentEffect short cache zdvojitCenu effectModificator children =
    Html.div
        [ Attrs.css
            [ Css.marginLeft (Css.rem 1)
            , Css.property "border-left" ("1px solid " ++ UI.varColMinorFg)
            ]
        ]
        [ viewEffectHeader short cache zdvojitCenu effectModificator children
        ]


wrapIf : Bool -> List (Html msg) -> Html msg
wrapIf cond =
    Html.div
        [ Attrs.css
            [ Css.displayFlex
            , Css.flexWrap Css.wrap
            , Css.property "gap" "0 0.5rem"
            ]
            |> UI.whenAttr cond
        ]


viewMainEffect : Bool -> DatabaseCache -> InnerEffect -> Html msg
viewMainEffect short cache effect =
    case effect of
        Pasivni attrs sleva ->
            viewPassive cache attrs sleva

        Utocna payload ->
            viewAttack short cache payload

        Kombo attack podminky ->
            viewCombo short cache attack podminky

        Obranna zasah stinZivot ->
            viewObranna short cache zasah stinZivot

        Leceni x ->
            viewLeceni cache x

        Neviditelnost x ->
            viewNeviditelnost x

        Vnimani x ->
            viewVnimani cache x

        Specialni ->
            Html.text ""


viewVnimani : DatabaseCache -> VnimaniPayload -> Html msg
viewVnimani cache payload =
    Html.div
        []
        [ UI.Definition.viewLabel "Vnímání"
        , DatabaseCache.viewName cache payload.typ
        , Html.text " "
        , UI.Definition.viewName (String.fromInt payload.lvl)
        ]


viewNeviditelnost : Int -> Html msg
viewNeviditelnost lvl =
    Html.div
        []
        [ UI.Definition.viewName "Skrytí"
        , Html.text " "
        , UI.Definition.viewName (String.fromInt lvl)
        ]


viewLeceni : DatabaseCache -> LeceniPayload -> Html msg
viewLeceni cache arg1 =
    let
        dmg : List String
        dmg =
            case arg1.dmg of
                Prave dmgValue ->
                    dmgToSuffix dmgValue

                Vsechny ->
                    [ "Všechna zranění" ]
    in
    case arg1.hesla of
        Prave hesla ->
            DatabaseCache.viewLabeledListShortCustom "a" "Léčení" cache hesla dmg

        Vsechny ->
            UI.Definition.stringListLabeled "a" "Léčení" ("Všechna hesla" :: dmg)


viewObranna : Bool -> DatabaseCache -> ZasahPayload -> Bool -> Html msg
viewObranna short cache arg1 stinZivot =
    if stinZivot then
        wrapIf short
            [ UI.Definition.stringLabeled "Dá sesilateli" (Just "Stínový život")
            , DatabaseCache.viewLabeledListShort "nebo" "Který lze použít proti útokům typu" cache arg1.zasah
            , DatabaseCache.viewLabeledListShortCustom "," "Při jeho využití navíc zahlaš útočníkovy" cache arg1.heslo (dmgToSuffix arg1.dmg ++ counterAttackType arg1)
            ]

    else
        wrapIf short
            [ DatabaseCache.viewLabeledListShort "nebo" "Když dostaneš útok typu" cache arg1.zasah
            , DatabaseCache.viewLabeledListShortCustom "," "Zahlaš útočníkovy" cache arg1.heslo (dmgToSuffix arg1.dmg ++ counterAttackType arg1)
            ]


counterAttackType : ZasahPayload -> List String
counterAttackType zasah =
    if GUID.onlyDefensive zasah.heslo && zasah.dmg == 0 then
        []

    else
        [ "Automaticky" ]


dmgToSuffix : Int -> List String
dmgToSuffix dmg =
    if dmg == 0 then
        []

    else
        [ String.fromInt dmg ++ " zranění" ]


viewCombo : Bool -> DatabaseCache -> AttackPayload -> PodminkyPayload -> Html msg
viewCombo short cache { attack, req } podminky =
    Html.div
        []
        [ wrapIf short
            [ DatabaseCache.viewLabeledListShort "nebo" "K útoku" cache attack.zasah
            , DatabaseCache.viewLabeledListShortCustom "a" "který už dává alespoň" cache podminky.reqHesla (dmgToSuffix podminky.reqDmg)
            , DatabaseCache.viewLabeledListShortCustom "a" "přidej" cache attack.heslo (dmgToSuffix attack.dmg)
            ]
        , UI.minor "(Nelze použít samostatně. Pouze přidat k základnímu útoku zbraní/rukou, nebo k technice která říká 'Proveď útok')"
            |> UI.whenHtml (not short)
        , viewAttackReq short cache req
        ]


viewAttack : Bool -> DatabaseCache -> AttackPayload -> Html msg
viewAttack short cache { attack, req } =
    Html.div
        []
        [ wrapIf short
            [ DatabaseCache.viewLabeledListShort "nebo" "Proveď útok" cache attack.zasah
            , DatabaseCache.viewLabeledListShortCustom "a" "který způsobí" cache attack.heslo (dmgToSuffix attack.dmg)
            ]
        , UI.whenHtml (attack.dmg == 0 && not short) <|
            UI.minor "(Tento útok sám o sobě nezpůsobuje žádné zranění)"
        , viewAttackReq short cache req
        ]


viewAttackReq : Bool -> DatabaseCache -> Maybe ZasahPayload -> Html msg
viewAttackReq short cache req =
    case req of
        Nothing ->
            Html.text ""

        Just reqZasah ->
            Html.div
                []
                [ UI.space (Css.rem 0.2)
                , UI.little "Tento útok lze použít pouze proti cíli kterému jsi bezprostředně předtím udělil:"
                    |> UI.whenHtml (not short)
                , UI.little "Kombo vyžaduje:"
                    |> UI.whenHtml short
                , Html.div
                    [ Attrs.css
                        [ Css.paddingLeft (Css.rem 1)
                        ]
                    ]
                    [ wrapIf short
                        [ DatabaseCache.viewLabeledListShort "nebo" "zásah typu" cache reqZasah.zasah
                        , DatabaseCache.viewLabeledListShortCustom "a" "který měl efekt alespoň" cache reqZasah.heslo (dmgToSuffix reqZasah.dmg)
                        ]
                    , UI.minor "(Tento zásah nemusel být platný, a nezálěží co na něj obránce zahlásil, jen musel fyzicky zasáhnout)"
                        |> UI.whenHtml (not short)
                    ]
                ]


viewPassive : DatabaseCache -> List Atribut -> Maybe SlevaPayload -> Html msg
viewPassive cache attrs sleva =
    let
        relevantAttrs : List (Html msg)
        relevantAttrs =
            DatabaseCache.viewValues "+" cache attrs
    in
    if List.length relevantAttrs > 0 || sleva /= Nothing then
        UI.col
            []
            [ UI.Definition.viewSimpleBlock "Technika pasivně přidává" relevantAttrs
            , case sleva of
                Just slev ->
                    viewSleva cache slev

                Nothing ->
                    Html.text ""
            ]

    else
        Html.text ""


viewSleva : DatabaseCache -> SlevaPayload -> Html msg
viewSleva cache sleva =
    UI.Definition.viewSimpleBlock "Technika pasivně zlevňuje sesílací cenu"
        (viewSlevaNormal cache sleva)


viewSlevaNormal : DatabaseCache -> SlevaPayload -> List (Html msg)
viewSlevaNormal cache sleva =
    viewSlevaSelector cache sleva.selector
        :: DatabaseCache.viewValues "-" cache sleva.cena


viewSlevaSelector : DatabaseCache -> TechnikaSelector -> Html msg
viewSlevaSelector cache selector =
    case selector of
        SelectVsechnyTechniky ->
            Html.text "Pro všechny techniky"

        SelectStrom ids ->
            DatabaseCache.viewLabeledListShort "a" "Pro techniky ve stromech" cache (GUID.setToList ids)

        SelectTechnika ids ->
            DatabaseCache.viewLabeledListShort "a" "Pro techniky" cache (GUID.setToList ids)


viewSlevaShort : DatabaseCache -> SlevaPayload -> List (Html msg)
viewSlevaShort cache sleva =
    viewSlevaSelectorShort cache sleva.selector
        :: DatabaseCache.viewValuesAlias "-" cache sleva.cena


viewSlevaSelectorShort : DatabaseCache -> TechnikaSelector -> Html msg
viewSlevaSelectorShort cache selector =
    case selector of
        SelectVsechnyTechniky ->
            Html.text "Všechny techniky"

        SelectStrom ids ->
            DatabaseCache.viewLabeledListShort "a" "Stromy" cache (GUID.setToList ids)

        SelectTechnika ids ->
            DatabaseCache.viewLabeledListShort "a" "Techniky" cache (GUID.setToList ids)
