module Jutsu exposing
    ( CheckboxOptions
    , Jutsu
    , PermalockReason(..)
    , access
    , activeEffectCount
    , decoder
    , extractTerms
    , givenAttributes
    , givenSleva
    , groupByLevel
    , id
    , level
    , tree
    , updateCache
    , viewCheckbox
    , viewDetail
    , viewTableContainer
    , viewTableHeader
    , viewTableRow
    )

import Css
import Css.Global
import DatabaseCache exposing (DatabaseCache)
import GUID exposing (GUID, GUIDSet)
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import Html.Styled.Lazy
import JutsuAccess exposing (JutsuAccess)
import JutsuEffect exposing (Effect)
import List.Extra
import UI
import UI.BlockQuote
import UI.Checkbox
import UI.Definition
import UI.Tooltip
import Utils exposing (Vsechny(..))
import Xml.Decode


type Jutsu
    = Jutsu JutsuPayload


type alias JutsuPayload =
    { id : GUID GUID.Jutsu
    , name : String
    , alias : List String
    , level : Int
    , typ : GUID GUID.JutsuType
    , modifikator : List (GUID GUID.Common)
    , tree : GUID GUID.Tree
    , flavour : String
    , cena : List ( GUID GUID.Common, Vsechny Int )
    , cooldown : Maybe (GUID GUID.Common)
    , access : JutsuAccess
    , prerekvizity : GUIDSet GUID.Jutsu
    , effects : List Effect
    , dbgRowId : Maybe Int
    }


id : Jutsu -> GUID GUID.Jutsu
id (Jutsu jutsu) =
    jutsu.id


name : Jutsu -> String
name (Jutsu jutsu) =
    jutsu.name


level : Jutsu -> Int
level (Jutsu jutsu) =
    jutsu.level


tree : Jutsu -> GUID GUID.Tree
tree (Jutsu payload) =
    payload.tree


access : Jutsu -> JutsuAccess
access (Jutsu jutsu) =
    jutsu.access


givenAttributes : Jutsu -> List JutsuEffect.Atribut
givenAttributes (Jutsu jutsu) =
    List.concatMap JutsuEffect.givenAttributes jutsu.effects


givenSleva : Jutsu -> List JutsuEffect.SlevaPayload
givenSleva (Jutsu jutsu) =
    List.filterMap JutsuEffect.givenSleva jutsu.effects


activeEffectCount : Jutsu -> Int
activeEffectCount (Jutsu jutsu) =
    jutsu.effects
        |> List.filter (not << JutsuEffect.isPassive)
        |> List.length


extractTerms : Jutsu -> GUIDSet GUID.Common
extractTerms (Jutsu jutsu) =
    jutsu.effects
        |> List.map JutsuEffect.extractTerms
        |> List.foldl GUID.setUnion GUID.setEmpty
        |> GUID.setUnion (GUID.setFromList (List.map Tuple.first jutsu.cena))
        |> GUID.setUnion (GUID.setFromMaybe jutsu.cooldown)
        |> GUID.setAdd (GUID.toCommon jutsu.typ)
        |> GUID.setUnion (GUID.setFromList jutsu.modifikator)



-- XML


decoder : GUID GUID.Tree -> Xml.Decode.Decoder Jutsu
decoder treeId =
    Xml.Decode.succeed JutsuPayload
        |> Xml.Decode.andMap GUID.idDecoder
        |> Xml.Decode.requiredPath [ "jméno" ] (Xml.Decode.single Xml.Decode.string)
        |> Xml.Decode.requiredPath [ "alias" ] (Xml.Decode.list Xml.Decode.string)
        |> Xml.Decode.requiredPath [ "úroveň" ] (Xml.Decode.single Xml.Decode.int)
        |> Xml.Decode.requiredPath [ "typ-efektu" ] (Xml.Decode.single GUID.refDecoder)
        |> Xml.Decode.andMap (GUID.refListPathDecoder [ "modifikátor-efektu" ])
        |> Xml.Decode.andMap (Xml.Decode.succeed treeId)
        |> Xml.Decode.optionalPath [ "flavour" ] (Xml.Decode.single Xml.Decode.string) ""
        |> Xml.Decode.andMap cenaDecoder
        |> Xml.Decode.optionalPath [ "cooldown" ] (Xml.Decode.single (Xml.Decode.map Just GUID.refDecoder)) Nothing
        |> Xml.Decode.andMap JutsuAccess.accessDecoder
        |> Xml.Decode.andMap (Xml.Decode.map GUID.setFromList (GUID.refListPathDecoder [ "prerekvizity", "technika" ]))
        |> Xml.Decode.andMap JutsuEffect.effectsDecoder
        |> Xml.Decode.andMap (Xml.Decode.maybe (Xml.Decode.intAttr "dbg-row-id"))
        |> Xml.Decode.map Jutsu


cenaDecoder : Xml.Decode.Decoder (List ( GUID GUID.Common, Vsechny Int ))
cenaDecoder =
    Xml.Decode.requiredPath [ "cena" ] (Xml.Decode.list GUID.refIntDecoder) (Xml.Decode.succeed identity)
        |> Xml.Decode.map (List.map (\( id_, int ) -> ( id_, Prave int )))
        |> Xml.Decode.andThen
            (\cena ->
                Xml.Decode.requiredPath [ "všechny-cena" ]
                    (Xml.Decode.list GUID.refDecoder)
                    (Xml.Decode.succeed
                        (List.map (\z -> ( z, Vsechny ))
                            >> (++) cena
                        )
                    )
            )



-- TRANSFORM


groupByLevel : List Jutsu -> List (List Jutsu)
groupByLevel techniky =
    List.Extra.gatherEqualsBy level techniky
        |> List.sortBy (Tuple.first >> level)
        |> List.map Utils.cons



-- CACHE


updateCache : Jutsu -> DatabaseCache -> DatabaseCache
updateCache (Jutsu jutsu) =
    DatabaseCache.registerJutsu jutsu.id
        { level = jutsu.level
        }
        { name = jutsu.name
        , details = ""
        , alias = Nothing
        }



-- VIEW CHECKBOX


type PermalockReason
    = None
    | CharacterLevel Int
    | Neschopnost
    | Element


permalockReasonMessage : PermalockReason -> String
permalockReasonMessage reason =
    case reason of
        None ->
            ""

        CharacterLevel 1 ->
            "🔒 Tato úroveň bude postavě přístupná až na příštím ročníku"

        CharacterLevel diff ->
            "🔒 Tato úroveň bude postavě přístupná až za " ++ String.fromInt diff ++ " ročníky"

        Neschopnost ->
            "🔒 Postava je v tomto stromě neschopná"

        Element ->
            "🔒 Postava neovládá tento element"


type alias CheckboxOptions msg =
    { selected : Bool
    , locked : Bool
    , permaLock : PermalockReason
    , relevantQuirks : GUIDSet GUID.QuirkMechanicky
    , onChange : Bool -> msg
    , isInUpperHalf : Bool
    }


viewCheckbox : DatabaseCache -> CheckboxOptions msg -> Jutsu -> Html msg
viewCheckbox cache options jutsu =
    UI.Tooltip.view
        { position = tooltipPositionFromLevel (level jutsu)
        , width = Css.rem 30
        , above = not options.isInUpperHalf
        }
        (UI.Checkbox.viewStylable
            { label = name jutsu ++ JutsuAccess.accessSuffix (access jutsu)
            , value = options.selected
            , editability = checkboxEditability options
            , onChange = options.onChange
            }
        )
        -- convert permaLock to string here, for lazy to work
        -- relevantQuirks are directly from model, so identity is stable
        (Html.Styled.Lazy.lazy5 viewDetail
            cache
            (permalockReasonMessage options.permaLock)
            True
            options.relevantQuirks
            jutsu
        )


checkboxEditability : CheckboxOptions msg -> UI.Checkbox.Editability
checkboxEditability options =
    if options.permaLock /= None then
        UI.Checkbox.PermaLocked

    else if options.locked then
        UI.Checkbox.Locked

    else
        UI.Checkbox.Editable


tooltipPositionFromLevel : Int -> UI.Tooltip.Position
tooltipPositionFromLevel lvl =
    if lvl < 3 then
        UI.Tooltip.Right

    else if lvl < 5 then
        UI.Tooltip.Center

    else
        UI.Tooltip.Left



-- VIEW DETAIL


wrapInParenthesis : String -> String
wrapInParenthesis text =
    if String.length text > 0 then
        "(" ++ text ++ ")"

    else
        ""


detailPadding : Css.Style
detailPadding =
    Css.padding2 (Css.rem 0.2) (Css.rem 0.4)


viewDetail : DatabaseCache -> String -> Bool -> GUIDSet GUID.QuirkMechanicky -> Jutsu -> Html msg
viewDetail cache permalockReason helpVisible relevantQuirks (Jutsu jutsu) =
    let
        ( inlineEffects, passiveEffects, activeEffects ) =
            JutsuEffect.separateInlinePassiveActive jutsu.effects
    in
    Html.div
        [ Attrs.css
            [ Css.displayFlex
            , Css.flexDirection Css.column
            , Css.property "gap" "0.4rem"
            , Css.position Css.relative
            , Css.property "max-width" "var(--jutsu-max-width, 30rem)"
            , Css.property "break-inside" "avoid"
            , UI.borderNormal
            , detailPadding
            , UI.whenCss (not helpVisible) <|
                showHelpOnlyOnHover
            ]
        ]
        [ Html.div
            [ Attrs.css
                [ Css.displayFlex
                , Css.flexDirection Css.column
                , Css.empty [ Css.display Css.none ]
                ]
            ]
            [ Html.text permalockReason
            , viewAccessDetail cache relevantQuirks jutsu
            ]
        , Html.div
            []
            [ Html.div
                [ Attrs.css
                    [ Css.displayFlex
                    , Css.flexWrap Css.wrap
                    , Css.property "gap" "0.5rem"
                    , Css.alignItems Css.baseline
                    ]
                ]
                (Html.b [] [ Html.text jutsu.name ]
                    :: List.map (wrapInParenthesis >> UI.gray) jutsu.alias
                )
            , viewMetadata cache jutsu
            ]
        , viewPrerekvizity cache jutsu.prerekvizity
        , UI.BlockQuote.view jutsu.flavour
        , Html.div
            []
            (List.map (JutsuEffect.view cache) inlineEffects)
        , viewCena cache jutsu
        , DatabaseCache.viewLabeledItemShort "Cooldown" cache jutsu.cooldown
        , viewTyp cache jutsu
        , UI.col
            []
            (List.map (JutsuEffect.view cache) passiveEffects)
        , viewActiveEffects cache jutsu.typ activeEffects
        , viewHelp cache helpVisible (extractTerms (Jutsu jutsu))
        , viewDbg jutsu
        ]


viewPrerekvizity : DatabaseCache -> GUIDSet GUID.Jutsu -> Html msg
viewPrerekvizity cache ids =
    let
        label : String
        label =
            if GUID.setLength ids == 1 then
                "Naučení vyžaduje znalost techniky"

            else
                "Naučení vyžaduje znalost technik"
    in
    DatabaseCache.viewLabeledListShort "a" label cache (GUID.setToList ids)


viewAccessDetail : DatabaseCache -> GUIDSet GUID.QuirkMechanicky -> JutsuPayload -> Html msg
viewAccessDetail cache relevantQuirks jutsu =
    case JutsuAccess.accessDetail cache relevantQuirks jutsu.access of
        Just accessDescription ->
            UI.minor accessDescription

        Nothing ->
            Html.text ""


viewDbg : JutsuPayload -> Html msg
viewDbg jutsu =
    Html.div
        [ Attrs.css
            [ Css.position Css.absolute
            , Css.top Css.zero
            , Css.right (Css.px 2)
            ]
        ]
        [ case jutsu.dbgRowId of
            Just rowId ->
                UI.minor (String.fromInt rowId)

            Nothing ->
                Html.text ""
        ]


viewTyp : DatabaseCache -> JutsuPayload -> Html msg
viewTyp cache jutsu =
    UI.Definition.stringLabeledWithAlias "Typ techniky"
        (DatabaseCache.name cache jutsu.typ)
        (jutsu.modifikator
            |> List.filterMap (DatabaseCache.name cache)
            |> String.join ", "
            |> Utils.justIfNotEmptyString
        )


viewMetadata : DatabaseCache -> JutsuPayload -> Html msg
viewMetadata cache jutsu =
    UI.row
        []
        [ UI.Definition.stringLabeledWithAlias "Úroveň techniky"
            (Just (Utils.levelToLetter jutsu.level))
            (Just (String.fromInt jutsu.level))
        , DatabaseCache.viewLabeledItemShortWithAlias "Strom" cache (Just jutsu.tree)
        ]


viewActiveEffects : DatabaseCache -> GUID GUID.JutsuType -> List Effect -> Html msg
viewActiveEffects cache typ activeEffects =
    case activeEffects of
        [] ->
            Html.text ""

        onlyOne :: [] ->
            JutsuEffect.view cache onlyOne

        many ->
            UI.col
                []
                (viewMultiEffectHelp typ
                    :: List.map (JutsuEffect.view cache) many
                )


viewMultiEffectHelp : GUID GUID.JutsuType -> Html msg
viewMultiEffectHelp typ =
    if GUID.equal GUID.const.efektPomocna typ then
        UI.little <|
            "Tuto techniku je možné seslat vícekrát. Konkrétní efekt lze vybrat až při využití."

    else if GUID.equal GUID.const.efektPasivni typ then
        Html.text ""

    else
        UI.little <|
            "Tuto techniku lze seslat pro jeden (pouze jeden) z následujících efektů."
                ++ " Konkrétní efekt je nutné vybrat již při seslání."


helpClassName : String
helpClassName =
    "elm--internal-cls--Jutsu-viewHelp-container"


showHelpOnlyOnHover : Css.Style
showHelpOnlyOnHover =
    Css.batch
        [ Css.Global.children
            [ Css.Global.class helpClassName
                [ Css.display Css.none
                ]
            ]
        , Css.hover
            [ Css.Global.children
                [ Css.Global.class helpClassName
                    [ Css.display Css.block
                    ]
                ]
            ]
        ]


viewHelp : DatabaseCache -> Bool -> GUIDSet a -> Html msg
viewHelp cache visible terms =
    let
        definedTerms : List (GUID a)
        definedTerms =
            List.filter (DatabaseCache.isDefined cache) (GUID.setToList terms)
    in
    if List.length definedTerms == 0 then
        Html.text ""

    else
        Html.div
            [ Attrs.css
                [ UI.textMinor
                , UI.whenCss (not visible) <|
                    Css.batch
                        [ UI.normal
                        , UI.textMinor
                        , UI.borderNormal
                        , detailPadding
                        , Css.borderTopWidth Css.zero
                        , Css.position Css.absolute
                        , Css.zIndex (Css.int 1)
                        , Css.top (Css.calc (Css.pct 100) Css.minus (Css.px 2))
                        , Css.property "left" ("-" ++ UI.borderNormalWidth)
                        , Css.property "right" ("-" ++ UI.borderNormalWidth)
                        , Css.boxShadow4 (Css.rem 0.1) (Css.rem 0.7) (Css.rem 0.5) (Css.hex "#000")
                        ]
                ]
            , Attrs.class helpClassName
            ]
            [ Html.hr
                [ Attrs.css
                    [ Css.property "border-color" UI.varColMinorFg
                    , Css.property "color" UI.varColMinorFg
                    ]
                ]
                []
            , UI.Definition.viewSimpleBlock "Nápověda"
                (List.map (DatabaseCache.viewDefinition cache) definedTerms)
            ]


viewCena : DatabaseCache -> JutsuPayload -> Html msg
viewCena cache jutsu =
    let
        values : List (Html msg)
        values =
            DatabaseCache.viewValuesVsechny "" cache jutsu.cena
    in
    if List.length values == 0 then
        Html.text ""

    else
        Html.div
            []
            [ UI.Definition.viewSimpleBlock "Sesílací cena" values
            , Html.div
                [ Attrs.css
                    [ UI.Definition.indentBlock
                    ]
                ]
                [ UI.minor "(Instatní/Reaktivní)"
                ]
                |> UI.whenHtml (peceteCost jutsu == Prave 0)
            ]


peceteCost : JutsuPayload -> Vsechny Int
peceteCost jutsu =
    jutsu.cena
        |> List.filter (Tuple.first >> GUID.equal GUID.const.cenaPecet)
        |> List.map Tuple.second
        |> List.head
        |> Maybe.withDefault (Prave 0)



-- VIEW AS TABLE ROW


tableCellStyle : Css.Style
tableCellStyle =
    Css.batch
        [ Css.padding2 (Css.rem 0.2) (Css.rem 0.5)
        , Css.verticalAlign Css.top
        , Css.property "border" ("1.4px solid " ++ UI.varColFg)
        , Css.property "break-inside" "avoid"
        ]


cssBgCol : Maybe String -> Css.Style
cssBgCol color =
    Maybe.map (\c -> Css.property "background-color" ("#" ++ c)) color
        |> Maybe.withDefault (Css.batch [])


verticalSep : Html msg
verticalSep =
    Html.div
        [ Attrs.css
            [ Css.property "border-left" ("2.4px solid " ++ UI.varColFg)
            , Css.marginRight (Css.rem 0.2)
            , Css.property "box-shadow" "0 0.2rem 0px 0px black, 0 -0.2rem 0px 0px black"
            ]
        ]
        []


viewTableContainer : List (Html msg) -> Html msg
viewTableContainer children =
    Html.table
        [ Attrs.css
            [ Css.borderCollapse Css.collapse
            , Css.boxSizing Css.borderBox
            , Css.maxWidth (Css.pct 100)
            ]
        ]
        children


viewTableHeader : DatabaseCache -> Maybe String -> GUID t -> Maybe (GUID GUID.Common) -> List (Html msg)
viewTableHeader cache color name_ pecet =
    [ Html.tr
        []
        [ Html.th
            [ Attrs.colspan (List.length columnNames)
            , Attrs.scope "col"
            , Attrs.css
                [ tableCellStyle
                , Css.borderTopWidth (Css.px 3)
                , Css.paddingTop (Css.rem 0.5)
                , cssBgCol color
                ]
            ]
            [ DatabaseCache.viewName cache name_
            , UI.gray
                (DatabaseCache.nameAlias cache name_
                    |> Maybe.map (wrapInParenthesis >> (++) " ")
                    |> Maybe.withDefault ""
                )
            , Maybe.map GUID.toString_HACK pecet
                |> Maybe.map
                    (\x ->
                        Html.span
                            [ Attrs.css [ Css.fontWeight Css.normal, Css.marginLeft (Css.rem 2) ] ]
                            [ UI.minor "Poslední pečeť: ", Html.text x ]
                    )
                |> Maybe.withDefault (Html.text "")
            ]
        ]
    , Html.tr
        []
        (List.map (viewTableContainerTH color) columnNames)
    ]


viewTableContainerTH : Maybe String -> String -> Html msg
viewTableContainerTH color colName =
    Html.th
        [ Attrs.scope "col"
        , Attrs.css
            [ tableCellStyle
            , Css.whiteSpace Css.noWrap
            , cssBgCol color
            ]
        ]
        [ UI.little colName
        ]


columnNames : List String
columnNames =
    List.map Tuple.first columnDef


columnVals : List (ColumnDef msg)
columnVals =
    List.map Tuple.second columnDef


type alias ColumnDef msg =
    ColDefParams -> Html msg


type alias ColDefParams =
    { j : Jutsu
    , jp : JutsuPayload
    , c : DatabaseCache
    }


columnDef : List ( String, ColumnDef msg )
columnDef =
    let
        m : Maybe String -> String
        m =
            Maybe.withDefault ""

        row : List (Html msg) -> Html msg
        row =
            Html.div
                [ Attrs.css
                    [ Css.displayFlex
                    , Css.property "gap" "0 0.5rem"
                    , Css.flexWrap Css.wrap
                    , Css.alignItems Css.baseline
                    ]
                ]

        rowSep : List (Html msg) -> Html msg
        rowSep children =
            Html.div
                [ Attrs.css
                    [ Css.displayFlex
                    , Css.property "gap" "0 0.5rem"
                    ]
                ]
                (List.intersperse verticalSep children)
    in
    [ ( "Úroveň"
      , \{ jp } ->
            Html.text
                (Utils.levelToLetter jp.level
                    ++ " "
                    ++ wrapInParenthesis (String.fromInt jp.level)
                )
      )
    , ( "Jméno"
      , \{ jp } ->
            row
                [ Html.text jp.name
                , UI.minor (m (Maybe.map wrapInParenthesis (List.head jp.alias)))
                ]
      )
    , ( "Typ"
      , \{ c, jp } ->
            row
                [ Html.text (m (DatabaseCache.name c jp.typ))
                , UI.gray (wrapInParenthesis (String.join ", " (List.map (DatabaseCache.name c >> m) jp.modifikator)))
                ]
      )
    , ( "Vyžaduje"
      , \{ c, jp } ->
            GUID.setToList jp.prerekvizity
                |> List.filterMap (DatabaseCache.name c)
                |> String.join ", "
                |> Html.text
      )
    , ( "Pasivně"
      , \{ c, j } -> row (DatabaseCache.viewValuesAlias "+" c (givenAttributes j))
      )
    , ( "Pasivně sleva"
      , \{ c, j } -> row (List.concatMap (JutsuEffect.viewSlevaShort c) (givenSleva j))
      )
    , ( "Cooldown"
      , \{ c, jp } -> Html.text (m (Maybe.andThen (DatabaseCache.nameAlias c) jp.cooldown))
      )
    , ( "Cena"
      , \{ c, jp } -> row (DatabaseCache.viewValuesAliasVsechny "" c jp.cena)
      )
    , ( "Efekt"
      , \{ c, jp } -> rowSep (List.map (JutsuEffect.viewShort c) (getActive jp.effects))
      )
    ]


getActive : List Effect -> List Effect
getActive eff =
    let
        ( _, _, a ) =
            JutsuEffect.separateInlinePassiveActive eff
    in
    a


wrapTableCell : Maybe String -> Html msg -> Html msg
wrapTableCell color children =
    Html.td
        [ Attrs.css
            [ tableCellStyle
            , cssBgCol color
            ]
        ]
        [ children ]


viewTableRow : DatabaseCache -> Maybe String -> Jutsu -> Html msg
viewTableRow cache color (Jutsu jp) =
    let
        params : { j : Jutsu, jp : JutsuPayload, c : DatabaseCache }
        params =
            { j = Jutsu jp, jp = jp, c = cache }

        view : ColumnDef msg -> Html msg
        view v =
            wrapTableCell color
                (v params)
    in
    Html.tr
        []
        (List.map view columnVals)
