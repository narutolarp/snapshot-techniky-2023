module GUID exposing
    ( Atribut
    , Base
    , Common
    , Element
    , GUID
    , GUIDDict
    , GUIDSet
    , HtmlSelectProps
    , Jutsu
    , JutsuType
    , Neschopnost
    , NeschopnostAny
    , QuirkMechanicky
    , Tree
    , Village
    , const
    , countEquals
    , dictEmpty
    , dictGet
    , dictMapTyp
    , dictSet
    , dictToItems
    , dump
    , encodeJson
    , encodeSimpleList
    , encodeSimpleListJson
    , equal
    , idDecoder
    , listFromMaybe
    , listSortBy
    , onlyDefensive
    , parseAgaints
    , pecetToImgUrl
    , refDecoder
    , refIntDecoder
    , refListPathDecoder
    , refListPathDecoderNonEmpty
    , setAdd
    , setAddIf
    , setAssign
    , setEmpty
    , setFromList
    , setFromMaybe
    , setHas
    , setIntersect
    , setLength
    , setRemove
    , setToList
    , setUnion
    , simpleListDecoder
    , simpleListDecoderJsonUntrusted
    , simpleListDecoderToSet
    , toCommon
    , toNeschopnost
    , toString_HACK
    , viewHtmlSelect
    , villageName_TODO
    )

import Dict exposing (Dict)
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import Html.Styled.Events as Events
import Html.Styled.Keyed as Keyed
import Json.Decode
import Json.Encode
import List.Extra
import Set exposing (Set)
import Xml.Decode
import XmlParser



-- GUIDDict


type GUIDDict typ a
    = GUIDDict (Dict String a)


dictEmpty : GUIDDict t a
dictEmpty =
    GUIDDict Dict.empty


dictGet : GUID t -> GUIDDict t a -> Maybe a
dictGet (GUID id) (GUIDDict dict) =
    Dict.get id dict


dictSet : GUID t -> a -> GUIDDict t a -> GUIDDict t a
dictSet (GUID id) a (GUIDDict dict) =
    GUIDDict (Dict.insert id a dict)


dictToItems : GUIDDict t a -> List ( GUID t, a )
dictToItems (GUIDDict dict) =
    Dict.toList dict
        |> List.map (Tuple.mapFirst GUID)


parseAgaints : GUIDDict t a -> String -> Maybe (GUID t)
parseAgaints (GUIDDict dict) rawId =
    if setHas undefinedIds (GUID rawId) then
        Just (GUID rawId)

    else
        Dict.get rawId dict
            |> Maybe.map (always (GUID rawId))


dictMapTyp : (GUID t1 -> GUID t2) -> GUIDDict t1 a -> GUIDDict t2 a
dictMapTyp _ (GUIDDict d) =
    GUIDDict d



-- GUIDSet


type GUIDSet typ
    = GUIDSet (Set String)


setEmpty : GUIDSet t
setEmpty =
    GUIDSet Set.empty


simpleListDecoderToSet : Xml.Decode.Decoder (GUIDSet t)
simpleListDecoderToSet =
    Xml.Decode.map setFromList simpleListDecoder


setAdd : GUID t -> GUIDSet t -> GUIDSet t
setAdd (GUID id) (GUIDSet set) =
    GUIDSet (Set.insert id set)


setAssign : GUIDSet t -> GUID t -> Bool -> GUIDSet t
setAssign set id predicate =
    if predicate then
        setAdd id set

    else
        setRemove id set


setAddIf : GUID t -> Bool -> GUIDSet t -> GUIDSet t
setAddIf id predicate set =
    if predicate then
        setAdd id set

    else
        set


setRemove : GUID t -> GUIDSet t -> GUIDSet t
setRemove (GUID id) (GUIDSet set) =
    GUIDSet (Set.remove id set)


setIntersect : GUIDSet t -> GUIDSet t -> GUIDSet t
setIntersect (GUIDSet a) (GUIDSet b) =
    GUIDSet (Set.intersect a b)


setUnion : GUIDSet t -> GUIDSet t -> GUIDSet t
setUnion (GUIDSet a) (GUIDSet b) =
    GUIDSet (Set.union a b)


setHas : GUIDSet t -> GUID t -> Bool
setHas (GUIDSet set) (GUID id) =
    Set.member id set


setLength : GUIDSet t -> Int
setLength (GUIDSet set) =
    Set.size set


setToList : GUIDSet t -> List (GUID t)
setToList (GUIDSet set) =
    List.map GUID (Set.toList set)


setFromList : List (GUID t) -> GUIDSet t
setFromList list =
    GUIDSet (Set.fromList (List.map destructPrivate list))


setFromMaybe : Maybe (GUID t) -> GUIDSet t
setFromMaybe item =
    case item of
        Nothing ->
            setEmpty

        Just id ->
            setFromList [ id ]



-- List GUID


refListPathDecoder : List String -> Xml.Decode.Decoder (List (GUID t))
refListPathDecoder path =
    Xml.Decode.path path (Xml.Decode.list refDecoder)


refListPathDecoderNonEmpty : List String -> Xml.Decode.Decoder (List (GUID t))
refListPathDecoderNonEmpty path =
    refListPathDecoder path
        |> Xml.Decode.andThen
            (\ids ->
                if List.length ids > 0 then
                    Xml.Decode.succeed ids

                else
                    Xml.Decode.fail "refList must not be empty"
            )


simpleListDecoder : Xml.Decode.Decoder (List (GUID t))
simpleListDecoder =
    Xml.Decode.string
        |> Xml.Decode.map
            (String.split " "
                >> List.map String.trim
                >> List.filter (not << String.isEmpty)
                >> List.map GUID
            )


encodeSimpleList : String -> List (GUID t) -> XmlParser.Node
encodeSimpleList elementName list =
    XmlParser.Element elementName
        []
        [ XmlParser.Text (String.join " " (List.map destructPrivate list))
        ]


listFromMaybe : Maybe (GUID t) -> List (GUID t)
listFromMaybe item =
    case item of
        Nothing ->
            []

        Just id ->
            [ id ]


simpleListDecoderJsonUntrusted : Json.Decode.Decoder (GUIDDict t a -> List (GUID t))
simpleListDecoderJsonUntrusted =
    let
        lazyValidate : List String -> GUIDDict t a -> List (GUID t)
        lazyValidate rawIds control =
            List.filterMap (parseAgaints control) rawIds
    in
    Json.Decode.string
        |> Json.Decode.map (String.split " " >> lazyValidate)


encodeSimpleListJson : List (GUID t) -> Json.Encode.Value
encodeSimpleListJson ids =
    List.map destructPrivate ids
        |> String.join " "
        |> Json.Encode.string


listSortBy : (a -> GUID t) -> List a -> List a
listSortBy getId list =
    List.sortBy (getId >> destructPrivate) list



-- GUID typ


toCommon : GUID a -> GUID Common
toCommon (GUID q) =
    GUID q


toNeschopnost : GUID (NeschopnostAny a) -> GUID Neschopnost
toNeschopnost (GUID q) =
    GUID q


type alias Base a =
    { a
        | guidTyp : ()
    }


type alias Common =
    Base {}


type alias QuirkMechanicky =
    Base { guidQuirkMechanickyTyp : () }


type alias NeschopnostAny a =
    Base { a | guidNeschopnostTyp : () }


type alias Neschopnost =
    NeschopnostAny {}


type alias Jutsu =
    Base { guidJutsuTyp : () }


type alias JutsuType =
    Base { guidJutsuTypeTyp : () }


type alias Tree =
    NeschopnostAny { guidTreeTyp : () }


type alias Atribut =
    Base { guidAtributTyp : () }


type alias Element =
    Base { guidElementTyp : () }


type alias Village =
    Base { guidVillageTyp : () }



-- GUID


type GUID typ
    = GUID String


destructPrivate : GUID t -> String
destructPrivate (GUID s) =
    s


toString_HACK : GUID t -> String
toString_HACK =
    destructPrivate


idDecoder : Xml.Decode.Decoder (GUID t)
idDecoder =
    Xml.Decode.map GUID
        (Xml.Decode.stringAttr "id")


refDecoder : Xml.Decode.Decoder (GUID t)
refDecoder =
    Xml.Decode.map GUID
        (Xml.Decode.stringAttr "ref")


refIntDecoder : Xml.Decode.Decoder ( GUID t, Int )
refIntDecoder =
    Xml.Decode.map2 Tuple.pair
        refDecoder
        Xml.Decode.int


equal : GUID t -> GUID t -> Bool
equal (GUID a) (GUID b) =
    a == b


countEquals : List (GUID t) -> List ( GUID t, Int )
countEquals idList =
    idList
        |> List.Extra.gatherEqualsBy destructPrivate
        |> List.map (Tuple.mapSecond (List.length >> (+) 1))


dump : GUID t -> Html msg
dump (GUID a) =
    Html.i
        []
        [ Html.text a
        ]


encodeJson : GUID t -> Json.Encode.Value
encodeJson (GUID raw) =
    Json.Encode.string raw



-- HTML


type alias HtmlSelectProps t msg =
    { options : List ( GUID t, String )
    , disabled : Bool
    , onChange : GUID t -> msg
    }


viewHtmlSelect : HtmlSelectProps t msg -> Maybe (GUID t) -> Html msg
viewHtmlSelect props selected =
    let
        emptyOption : List ( String, Html msg )
        emptyOption =
            if selected == Nothing then
                [ ( "", Html.option [] [] )
                ]

            else
                []
    in
    Keyed.node "select"
        [ Maybe.map (destructPrivate >> Attrs.value) selected
            |> Maybe.withDefault (Attrs.class "")
        , Attrs.disabled props.disabled
        , Events.onInput (GUID >> props.onChange)
        ]
        (emptyOption ++ List.map (viewHtmlSelectItem selected) props.options)


viewHtmlSelectItem : Maybe (GUID t) -> ( GUID t, String ) -> ( String, Html msg )
viewHtmlSelectItem selected ( GUID idPayload, label ) =
    let
        isSelected : Bool
        isSelected =
            case selected of
                Just (GUID rawSelected) ->
                    idPayload == rawSelected

                Nothing ->
                    False
    in
    ( idPayload
    , Html.option
        [ Attrs.value idPayload
        , Attrs.selected isSelected
        ]
        [ Html.text label
        ]
    )



-- HARDCODED VALUES


{-| HACK: hardcoded test
-}
onlyDefensive : List (GUID t) -> Bool
onlyDefensive list =
    case list of
        [ GUID "úhyb" ] ->
            True

        [] ->
            True

        _ ->
            False


{-| HACK: hardcoded constants
-}
const :
    { leceni : GUID Common
    , stinZivot : GUID Common
    , skryti : GUID Common
    , vnimani : GUID Common
    , followupEfekt : GUID Common
    , efektHlavni : GUID JutsuType
    , efektPosileni : GUID JutsuType
    , efektPomocna : GUID JutsuType
    , efektPasivni : GUID JutsuType
    , neschopnostElementy : GUID Neschopnost
    , cenaPecet : GUID Common
    , villages : List (GUID Village)
    }
const =
    { leceni = GUID "léčení"
    , stinZivot = GUID "stínový-život"
    , skryti = GUID "skrytí"
    , vnimani = GUID "vnímání"
    , followupEfekt = GUID "kombo-efekt"
    , efektHlavni = GUID "hlavní"
    , efektPosileni = GUID "posílení"
    , efektPomocna = GUID "pomocná"
    , efektPasivni = GUID "pasivní"
    , neschopnostElementy = GUID "all-element-trees"
    , cenaPecet = GUID "pečeť"
    , villages =
        [ GUID "cloud"
        , GUID "dill"
        , GUID "eddy"
        , GUID "frost"
        , GUID "grass"
        , GUID "leaf"
        , GUID "mist"
        , GUID "rain"

        -- , GUID "samurai"  ID CONFLICT
        , GUID "sand"
        , GUID "sound"
        , GUID "spring"
        , GUID "stone"
        , GUID "valley"
        , GUID "waterfall"
        ]
    }


villageName_TODO : GUID Village -> String
villageName_TODO (GUID id) =
    case id of
        "cloud" ->
            "Oblačná"

        "dill" ->
            "Koprová"

        "eddy" ->
            "Mořské víry"

        "frost" ->
            "Mrazová"

        "grass" ->
            "Travní"

        "leaf" ->
            "Listová"

        "mist" ->
            "Mlžná"

        "rain" ->
            "Deštná"

        "samurai" ->
            "Samurajové"

        "sand" ->
            "Písečná"

        "sound" ->
            "Zvučná"

        "spring" ->
            "Horké prameny"

        "stone" ->
            "Kamenná"

        "valley" ->
            "Údolní"

        "waterfall" ->
            "Vodopádová"

        unknown ->
            unknown


{-| HACK: ids that are not defined in xml, but are used in application
-}
undefinedIds : GUIDSet Neschopnost
undefinedIds =
    setFromList
        [ const.neschopnostElementy
        ]


pecetToImgUrl : GUID Common -> String
pecetToImgUrl (GUID id) =
    "https://narutolarp.cz/images/seal_" ++ pecetTranslate id ++ ".png"


pecetTranslate : String -> String
pecetTranslate p =
    case p of
        "střet" ->
            "stret"

        "základ" ->
            "base"

        "pes" ->
            "dog"

        "beran" ->
            "ram"

        "opice" ->
            "monkey"

        "zajíc" ->
            "hare"

        "býk" ->
            "ox"

        "prase" ->
            "boar"

        "had" ->
            "serpent"

        "tygr" ->
            "tiger"

        "drak" ->
            "dragon"

        "krysa" ->
            "rat"

        "kohout" ->
            "bird"

        "kůň" ->
            "horse"

        _ ->
            ""
