module DatabaseCache exposing
    ( DatabaseCache
    , GetAllWithName
    , JutsuPayload
    , QuirkPayload
    , RegisterSpecific
    , SimpleListDecoderUntrusted
    , SingleDecoderUntrusted
    , StringValidate
    , TreePayload
    , atributDecoder
    , commonDecoder
    , commonPayload
    , details
    , elementDecoder
    , elementDecoderJsonUntrusted
    , elements
    , empty
    , isDefined
    , name
    , nameAlias
    , quirkPayload
    , registerCommon
    , registerIterHelper
    , registerJutsu
    , registerQuirk
    , registerTree
    , simpleJutsuListDecoderJsonUntrusted
    , simpleListDecoderJsonUntrustedSpecialni
    , simpleNeschopnostListDecoderJsonUntrusted
    , stringValidateSpecialniSchopnost
    , stringValidateTree
    , sumAttributesWithDefaults
    , viewCommonBlock
    , viewDefinition
    , viewLabeledItemShort
    , viewLabeledItemShortWithAlias
    , viewLabeledListShort
    , viewLabeledListShortCustom
    , viewLabeledListShortString
    , viewName
    , viewValues
    , viewValuesAlias
    , viewValuesVsechny, viewValuesAliasVsechny
    )

import Css
import GUID exposing (GUID, GUIDDict, GUIDSet)
import Html.Styled as Html exposing (Html)
import Json.Decode
import List.Extra
import UI
import UI.Definition
import Utils exposing (Vsechny(..))
import Xml.Decode


type DatabaseCache
    = DatabaseCache DatabaseCachePayload


type alias DatabaseCachePayload =
    { common : GUIDDict GUID.Common CommonPayload
    , jutsu : GUIDDict GUID.Jutsu JutsuPayload
    , tree : GUIDDict GUID.Tree TreePayload
    , atribut : GUIDDict GUID.Atribut AtributPayload
    , element : GUIDDict GUID.Element ElementPayload
    , specialniSchopnost : GUIDDict GUID.QuirkMechanicky QuirkPayload
    , idConflicts : GUIDSet GUID.Common
    }


payload : DatabaseCache -> DatabaseCachePayload
payload (DatabaseCache cache) =
    cache


type alias CommonPayload =
    { name : String
    , details : String
    , alias : Maybe String
    }


type alias TreePayload =
    {}


type alias JutsuPayload =
    { level : Int
    }


type alias AtributPayload =
    { defaultValue : Int
    }


type alias ElementPayload =
    { name : String
    }


type alias QuirkPayload =
    { element : GUIDSet GUID.Element
    , kekkeiGenkai : Bool
    , spojujeStromy : GUIDSet GUID.Tree
    }


empty : DatabaseCache
empty =
    DatabaseCache
        { common = GUID.dictEmpty
        , jutsu = GUID.dictEmpty
        , atribut = GUID.dictEmpty
        , element = GUID.dictEmpty
        , tree = GUID.dictEmpty
        , specialniSchopnost = GUID.dictEmpty
        , idConflicts = GUID.setEmpty
        }


isDefined : DatabaseCache -> GUID t -> Bool
isDefined (DatabaseCache cache) id =
    case GUID.dictGet (GUID.toCommon id) cache.common of
        Just _ ->
            True

        Nothing ->
            False


getValue :
    (DatabaseCachePayload -> GUIDDict t2 payload)
    -> (payload -> value)
    -> (GUID t1 -> GUID t2)
    -> DatabaseCache
    -> GUID t1
    -> Maybe value
getValue dictAccessor valueAccessor mapId (DatabaseCache cache) id =
    GUID.dictGet (mapId id) (dictAccessor cache)
        |> Maybe.map valueAccessor


name : DatabaseCache -> GUID a -> Maybe String
name =
    getValue .common .name GUID.toCommon


nameAlias : DatabaseCache -> GUID a -> Maybe String
nameAlias cache id =
    getValue .common .alias GUID.toCommon cache id
        |> Maybe.andThen identity


details : DatabaseCache -> GUID a -> Maybe String
details =
    getValue .common .details GUID.toCommon


commonPayload : DatabaseCache -> GUID a -> Maybe CommonPayload
commonPayload =
    getValue .common identity GUID.toCommon


quirkPayload : DatabaseCache -> GUID GUID.QuirkMechanicky -> Maybe QuirkPayload
quirkPayload =
    getValue .specialniSchopnost identity identity



-- VALIDATE


type alias StringValidate guidTyp =
    DatabaseCache -> String -> Maybe (GUID guidTyp)


stringValidate : (DatabaseCachePayload -> GUIDDict t a) -> StringValidate t
stringValidate getDict (DatabaseCache cache) value =
    GUID.parseAgaints (getDict cache) value


stringValidateTree : StringValidate GUID.Tree
stringValidateTree =
    stringValidate .tree


stringValidateSpecialniSchopnost : StringValidate GUID.QuirkMechanicky
stringValidateSpecialniSchopnost =
    stringValidate .specialniSchopnost


type alias SimpleListDecoderUntrusted guidTyp =
    Json.Decode.Decoder (DatabaseCache -> List (GUID guidTyp))


simpleListDecoderJsonUntrustedBase : (DatabaseCachePayload -> GUIDDict t a) -> Json.Decode.Decoder (DatabaseCache -> List (GUID t))
simpleListDecoderJsonUntrustedBase getValidateDict =
    let
        lazyCacheToDict : (GUIDDict t a -> List (GUID t)) -> DatabaseCache -> List (GUID t)
        lazyCacheToDict validate (DatabaseCache cache) =
            validate (getValidateDict cache)
    in
    GUID.simpleListDecoderJsonUntrusted
        |> Json.Decode.map lazyCacheToDict


simpleJutsuListDecoderJsonUntrusted : SimpleListDecoderUntrusted GUID.Jutsu
simpleJutsuListDecoderJsonUntrusted =
    simpleListDecoderJsonUntrustedBase .jutsu


simpleNeschopnostListDecoderJsonUntrusted : SimpleListDecoderUntrusted GUID.Neschopnost
simpleNeschopnostListDecoderJsonUntrusted =
    Json.Decode.map2 (\a b cache -> a cache ++ b cache)
        (simpleListDecoderJsonUntrustedBase (always getValidateDictElementNeschopnost))
        (simpleListDecoderJsonUntrustedBase (.tree >> GUID.dictMapTyp GUID.toNeschopnost))


getValidateDictElementNeschopnost : GUIDDict GUID.Neschopnost ()
getValidateDictElementNeschopnost =
    GUID.dictSet GUID.const.neschopnostElementy () GUID.dictEmpty


simpleListDecoderJsonUntrustedSpecialni : SimpleListDecoderUntrusted GUID.QuirkMechanicky
simpleListDecoderJsonUntrustedSpecialni =
    simpleListDecoderJsonUntrustedBase .specialniSchopnost


type alias SingleDecoderUntrusted t =
    Json.Decode.Decoder (DatabaseCache -> Maybe (GUID t))


singleDecoderJsonUntrusted : (DatabaseCachePayload -> GUIDDict t a) -> Json.Decode.Decoder (DatabaseCache -> Maybe (GUID t))
singleDecoderJsonUntrusted getValidateDict =
    Json.Decode.string
        |> Json.Decode.map (\raw cache -> GUID.parseAgaints (getValidateDict (payload cache)) raw)


elementDecoderJsonUntrusted : SingleDecoderUntrusted GUID.Element
elementDecoderJsonUntrusted =
    singleDecoderJsonUntrusted .element



-- GET ALL


type alias GetAllWithName t =
    DatabaseCache -> List ( GUID t, String )


getAllWithName : (DatabaseCachePayload -> GUIDDict t p) -> GetAllWithName t
getAllWithName getter (DatabaseCache cache) =
    let
        withName : ( GUID t, p ) -> Maybe ( GUID t, String )
        withName ( id, _ ) =
            GUID.dictGet (GUID.toCommon id) cache.common
                |> Maybe.map (\common -> ( id, common.name ))
    in
    getter cache
        |> GUID.dictToItems
        |> List.filterMap withName


elements : GetAllWithName GUID.Element
elements =
    getAllWithName .element



-- ATTRIBUTES


getAttributeDefaults : DatabaseCache -> List ( GUID GUID.Atribut, Int )
getAttributeDefaults (DatabaseCache cache) =
    GUID.dictToItems cache.atribut
        |> List.map (Tuple.mapSecond .defaultValue)


sumAttributesWithDefaults : DatabaseCache -> List ( GUID GUID.Atribut, Int ) -> List ( GUID GUID.Atribut, Int )
sumAttributesWithDefaults cache values =
    GUID.dictEmpty
        |> sumAttributesWithDefaultsIter (getAttributeDefaults cache)
        |> sumAttributesWithDefaultsIter values
        |> GUID.dictToItems


sumAttributesWithDefaultsIter : List ( GUID GUID.Atribut, Int ) -> GUIDDict GUID.Atribut Int -> GUIDDict GUID.Atribut Int
sumAttributesWithDefaultsIter items dict =
    case items of
        ( id, value ) :: rest ->
            dict
                |> GUID.dictGet id
                |> Maybe.withDefault 0
                |> (+) value
                |> (\v -> GUID.dictSet id v dict)
                |> sumAttributesWithDefaultsIter rest

        [] ->
            dict



-- REGISTER


registerIterHelper : (a -> DatabaseCache -> DatabaseCache) -> List a -> DatabaseCache -> DatabaseCache
registerIterHelper registerFn items cache =
    List.foldl registerFn cache items


type alias RegisterCommon =
    CommonPayload -> DatabaseCache -> DatabaseCache


type alias RegisterSpecific typ payload =
    GUID typ -> payload -> RegisterCommon


registerCommon : GUID t -> RegisterCommon
registerCommon id common (DatabaseCache cache) =
    let
        commonId : GUID GUID.Common
        commonId =
            GUID.toCommon id
    in
    DatabaseCache
        { cache
            | common = GUID.dictSet commonId common cache.common
            , idConflicts = GUID.setAddIf commonId (GUID.dictGet commonId cache.common /= Nothing) cache.idConflicts
        }


registerCommonCompat : RegisterSpecific t ()
registerCommonCompat id _ =
    registerCommon id


registerSpecific : (DatabaseCachePayload -> GUIDDict typ payload) -> (GUIDDict typ payload -> DatabaseCachePayload -> DatabaseCachePayload) -> RegisterSpecific typ payload
registerSpecific get set id specific common (DatabaseCache cache) =
    DatabaseCache (set (GUID.dictSet id specific (get cache)) cache)
        |> registerCommon id common


registerJutsu : RegisterSpecific GUID.Jutsu JutsuPayload
registerJutsu =
    registerSpecific .jutsu (\dict cache -> { cache | jutsu = dict })


registerAtribut : RegisterSpecific GUID.Atribut AtributPayload
registerAtribut =
    registerSpecific .atribut (\dict cache -> { cache | atribut = dict })


registerElement : RegisterSpecific GUID.Element ElementPayload
registerElement =
    registerSpecific .element (\dict cache -> { cache | element = dict })


registerTree : RegisterSpecific GUID.Tree TreePayload
registerTree =
    registerSpecific .tree (\dict cache -> { cache | tree = dict })


registerQuirk : RegisterSpecific GUID.QuirkMechanicky QuirkPayload
registerQuirk =
    registerSpecific .specialniSchopnost (\dict cache -> { cache | specialniSchopnost = dict })



-- DECODE


commonDecoder : String -> DatabaseCache -> Xml.Decode.Decoder DatabaseCache
commonDecoder =
    definiceDecoder (Xml.Decode.succeed ()) registerCommonCompat


atributDecoder : String -> DatabaseCache -> Xml.Decode.Decoder DatabaseCache
atributDecoder =
    definiceDecoder atributPayloadDecoder registerAtribut


atributPayloadDecoder : Xml.Decode.Decoder AtributPayload
atributPayloadDecoder =
    Xml.Decode.map AtributPayload
        (Xml.Decode.intAttr "default")


elementDecoder : String -> DatabaseCache -> Xml.Decode.Decoder DatabaseCache
elementDecoder =
    definiceDecoder elementPayloadDecoder registerElement


elementPayloadDecoder : Xml.Decode.Decoder ElementPayload
elementPayloadDecoder =
    Xml.Decode.map (\common -> ElementPayload common.name) commonPayloadDecoder


definiceDecoder :
    Xml.Decode.Decoder a
    -> (GUID t -> a -> CommonPayload -> DatabaseCache -> DatabaseCache)
    -> String
    -> DatabaseCache
    -> Xml.Decode.Decoder DatabaseCache
definiceDecoder specificDecoder register_ tag cache =
    Xml.Decode.with
        (Xml.Decode.path [ tag ] (Xml.Decode.list (definiceItemDecoder specificDecoder)))
        (Xml.Decode.succeed << List.foldl (\( id, ( common, specific ) ) -> register_ id specific common) cache)


definiceItemDecoder : Xml.Decode.Decoder a -> Xml.Decode.Decoder ( GUID t, ( CommonPayload, a ) )
definiceItemDecoder specificDecoder =
    Xml.Decode.map2 Tuple.pair
        GUID.idDecoder
        (Xml.Decode.map2 Tuple.pair
            commonPayloadDecoder
            specificDecoder
        )


commonPayloadDecoder : Xml.Decode.Decoder CommonPayload
commonPayloadDecoder =
    Xml.Decode.map3 CommonPayload
        (Xml.Decode.path [ "jméno" ] (Xml.Decode.single Xml.Decode.string))
        (Xml.Decode.path [ "popisek" ] (Xml.Decode.single Xml.Decode.string))
        (Xml.Decode.path [ "alias" ] (Xml.Decode.single Xml.Decode.string)
            |> Xml.Decode.maybe
        )



-- VIEW


isNonZero : Vsechny Int -> Bool
isNonZero x =
    case x of
        Vsechny ->
            True

        Prave intVal ->
            intVal /= 0


viewValuesVsechny : String -> DatabaseCache -> List ( GUID t, Vsechny Int ) -> List (Html msg)
viewValuesVsechny prefix cache list =
    list
        |> List.filter (Tuple.second >> isNonZero)
        |> List.map (viewSingleValueVsechny prefix cache)


viewValuesAliasVsechny : String -> DatabaseCache -> List ( GUID t, Vsechny Int ) -> List (Html msg)
viewValuesAliasVsechny prefix cache list =
    list
        |> List.filter (Tuple.second >> isNonZero)
        |> List.map (viewSingleValueAliasVsechny prefix cache)


vsechnyToString : Vsechny Int -> String
vsechnyToString val =
    case val of
        Vsechny ->
            "Všechny"

        Prave valInt ->
            String.fromInt valInt


viewSingleValueVsechny : String -> DatabaseCache -> ( GUID t, Vsechny Int ) -> Html msg
viewSingleValueVsechny prefix cache ( id, val ) =
    Html.div
        []
        [ viewLabel cache id
        , UI.Definition.viewName (prefix ++ vsechnyToString val)
        ]


viewSingleValueAliasVsechny : String -> DatabaseCache -> ( GUID t, Vsechny Int ) -> Html msg
viewSingleValueAliasVsechny prefix cache ( id, val ) =
    Html.div
        []
        [ viewLabelAlias cache id
        , UI.Definition.viewName (prefix ++ vsechnyToString val)
        ]


viewValues : String -> DatabaseCache -> List ( GUID t, Int ) -> List (Html msg)
viewValues prefix cache list =
    viewValuesVsechny prefix cache (List.map (Tuple.mapSecond Prave) list)


viewValuesAlias : String -> DatabaseCache -> List ( GUID t, Int ) -> List (Html msg)
viewValuesAlias prefix cache list =
    viewValuesAliasVsechny prefix cache (List.map (Tuple.mapSecond Prave) list)


viewDefinition : DatabaseCache -> GUID t -> Html msg
viewDefinition cache id =
    Html.div
        []
        [ viewSingleValueAs .name UI.Definition.viewName cache id
        , Html.text ": "
        , viewSingleValueAs .details Html.text cache id
        ]


viewLabeledItemShortWithAlias : String -> DatabaseCache -> Maybe (GUID t) -> Html msg
viewLabeledItemShortWithAlias label cache item =
    UI.Definition.stringLabeledWithAlias label
        (Maybe.andThen (name cache) item)
        (Maybe.andThen (nameAlias cache) item)


viewLabeledItemShort : String -> DatabaseCache -> Maybe (GUID t) -> Html msg
viewLabeledItemShort label cache item =
    viewLabeledListShortCustom "" label cache (GUID.listFromMaybe item) []


viewLabeledListShortString : String -> String -> DatabaseCache -> List (GUID a) -> String
viewLabeledListShortString sep label cache list =
    (label ++ ": ")
        ++ (case List.Extra.unconsLast (List.filterMap (name cache) list) of
                Just ( last, first :: body ) ->
                    String.join ", " (first :: body)
                        ++ addSpacesToSeparator sep
                        ++ last

                Just ( last, [] ) ->
                    last

                Nothing ->
                    ""
           )


viewLabeledListShort : String -> String -> DatabaseCache -> List (GUID t) -> Html msg
viewLabeledListShort sep label cache list =
    viewLabeledListShortCustom sep label cache list []


viewLabeledListShortCustom : String -> String -> DatabaseCache -> List (GUID t) -> List String -> Html msg
viewLabeledListShortCustom sep label cache list suffix =
    UI.Definition.listLabeled sep
        label
        (List.map (viewNameCounted cache) (GUID.countEquals list)
            ++ List.map UI.Definition.viewName suffix
        )


addSpacesToSeparator : String -> String
addSpacesToSeparator sep =
    case String.trim sep of
        "," ->
            ", "

        _ ->
            " " ++ sep ++ " "


viewName : DatabaseCache -> GUID t -> Html msg
viewName =
    viewSingleValueAs .name UI.Definition.viewName


viewNameCounted : DatabaseCache -> ( GUID t, Int ) -> Html msg
viewNameCounted cache ( id, count ) =
    UI.Definition.multiply ( viewName cache id, count )


viewLabel : DatabaseCache -> GUID t -> Html msg
viewLabel =
    viewSingleValueAs .name UI.Definition.viewLabel


viewLabelAlias : DatabaseCache -> GUID t -> Html msg
viewLabelAlias =
    viewSingleValueAs (\v -> v.alias |> Maybe.withDefault v.name) UI.Definition.viewLabel


viewSingleValueAs : (CommonPayload -> value) -> (value -> Html msg) -> DatabaseCache -> GUID t -> Html msg
viewSingleValueAs accessor element cache id =
    case getValue .common accessor GUID.toCommon cache id of
        Just value ->
            element value

        Nothing ->
            GUID.dump id


viewCommonBlock : DatabaseCache -> GUID t -> Html msg
viewCommonBlock cache id =
    case commonPayload cache id of
        Just data ->
            Html.div
                []
                [ UI.Definition.viewName data.name
                , case data.alias of
                    Just alias_ ->
                        UI.minor (" (" ++ alias_ ++ ")")

                    Nothing ->
                        Html.text ""
                , UI.space (Css.rem 0.1)
                , Html.text data.details
                ]

        Nothing ->
            Html.div
                []
                [ GUID.dump id
                ]
