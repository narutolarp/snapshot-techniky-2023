const BUTTON_CLASSES = ['minor', 'primary'];

const styles = `
button {
    border: none;
    background-color: transparent;
    cursor: pointer;
}
button:hover {
    text-decoration: underline;
}
button.primary {
    font-size: 1.1em;
    padding: 0.4rem 1rem;
    border-radius: 0.5em 0;
    background-color: black;
    color: white;
}
button.minor {
    color: var(--col--minor-fg);
}
`;


export class DialogControlButton extends HTMLElement {
    #button;

    constructor() {
        super();
        this.attachShadow({ mode: 'open' });

        this.#button = document.createElement('button');
        this.#button.type = 'button';
        this.#button.addEventListener('click', this.#onClick);
        this.shadowRoot.appendChild(this.#button);

        const styleElement = document.createElement('style');
        styleElement.innerText = styles;
        this.shadowRoot.appendChild(styleElement);
    }

    set btnlabel(text) {
        this.#button.innerText = text;
    }

    set btnclass(cls) {
        if (!BUTTON_CLASSES.includes(cls)) {
            throw new Error(`invalid btnclass="${cls}"`);
        }
        this.#button.className = cls;
    }

    #getDialog() {
        return this.parentElement.parentElement.querySelector('dialog');
    }

    #onClick = event => {
        const btnaction = this.getAttribute('btnaction');
        switch (btnaction) {
            case 'open':
                this.#getDialog().showModal();
                break;
            case 'close':
                this.#getDialog().close();
                break;
            default:
                throw new Error(`invalid btnaction="${btnaction}"`);
        }
    }
}


customElements.define('dialog-control-button', DialogControlButton);
